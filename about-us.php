﻿<?php 
include "admin/database.php";
 $sql1 ="SELECT name, mobile, image, heading from profile";
 $sql2 ="SELECT name, wishes from greetings";
 $result1 = $conn->query($sql1);
 $result2 = $conn->query($sql2);
 if($result1->num_rows > 0)
{
    $profile = $result1->fetch_assoc();
}
 if($result2->num_rows > 0)
{
   $profile1 = $result2->fetch_assoc();
 }
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>About Us - GS COP Ltd</title>
      <meta name="description" content="Your website description." />
      <meta name="keywords" content="other" />
      <meta name="generator" content="Parallels Web Presence Builder 11.0.14" />
      <link rel="stylesheet" type="text/css" href="css/style.css" />
      <!--[if IE 7]>
      <link rel="stylesheet" type="text/css" href="../css/ie7.css?template=generic" />
      <![endif]-->
      <script language="JavaScript" type="text/javascript" src="js/style-fix.js"></script>
      <script language="JavaScript" type="text/javascript" src="js/css_browser_selector.js"></script>
      <link type="text/css" href="css/breadcrumbs-8685e8f3-5244-eb19-e021-18a5ed6b824a.css" rel="stylesheet" />
      <link type="text/css" href="css/header-9f0de22c-309d-01e2-c041-eb8e0648ccc6.css" rel="stylesheet" />
      <link type="text/css" href="css/navigation-617ca861-db53-45a1-27f0-a036f4a59f85.css" rel="stylesheet" />
      <script type="text/javascript">var addthis_config = {
         ui_language: 'en'
         };
      </script><script type="text/javascript" src="js/250/addthis_widget.js"></script>
      <script type="text/javascript">addthis.addEventListener('addthis.ready', function() {
         for (var i in addthis.links) {
         	var link = addthis.links[i];
         	if (link.className.indexOf("tweet") > -1) {
         		var iframe = link.firstChild;
         		if (iframe.src.indexOf("http://") !== 0) {
         			iframe.src = iframe.src.replace(/^(\/\/|https:\/\/)/, 'http://');
         		}
         	}
         }
         });
      </script><script type="text/javascript" src="components/jquery/jquery.js"></script>
      <script type="text/javascript" src="components/jquery/jquery.jcarousellite.js"></script>
      <script type="text/javascript" src="components/jquery/jquery.simplemodal.js"></script>
      <script type="text/javascript" src="modules/imagegallery/imagegallery.js"></script>
      <link type="text/css" href="modules/imagegallery/imagegallery.css" rel="stylesheet" />
      <script type="text/javascript" src="modules/imagegallery/imagegallery.locale-en_US.js"></script>
      <!--[if IE]>
      <style type="text/css">.background23-white{filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="../images/background23-white.png?template=generic",sizingmethod=scale,enabled=true);} </style>
      <![endif]-->   
      <!--<style type="text/css">
         body{background-image: url("attachments/Background/asd.jpg");
         background-position: top left;
         background-repeat:repeat;}
         
         </style>-->
      <style type="text/css"></style>
      <!--[if IE]>
      <meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT" />
      <![endif]-->
      <link rel="shortcut icon" href="favicon.ico">
      <script type="text/javascript">var siteBuilderJs = jQuery.noConflict(true);</script><script type="text/javascript">siteBuilderJs(document).ready(function($){
         $("#widget-eb1185e6-5be9-4b90-c7c7-7227aa3582e2 .widget-content").imagegallery({
         	thumbSize: 'small',
         	pageSize: 1,
         	isShowFullSize: true,
         	isRtl: false,
         	openImageCallback: function(gallery) {
         		$('#imageGalleryFullSizeButton').click(function() {
         			window.open(gallery.currentImage.url);
         		});
         	},
         	store: {"type":"array","data":[{"id":"246ba7b9-a176-ff7f-8ebd-bad202f213ec","title":"Img2016","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/246ba7b9-a176-ff7f-8ebd-bad202f213ec.jpg","thumbUrl":"data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/246ba7b9-a176-ff7f-8ebd-bad202f213ec.thumb.jpg"},{"id":"8ef98089-8794-416c-57be-c6d8821d4dd5","title":"Img2016 (6)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/8ef98089-8794-416c-57be-c6d8821d4dd5.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/8ef98089-8794-416c-57be-c6d8821d4dd5.thumb.jpg"},{"id":"b9d5db5a-bc62-2e05-3205-d0fb185eec68","title":"Img2016 (5)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/b9d5db5a-bc62-2e05-3205-d0fb185eec68.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/b9d5db5a-bc62-2e05-3205-d0fb185eec68.thumb.jpg"},{"id":"c11d2812-c926-fbef-b191-69b6e7781c1c","title":"Img2016 (4)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c11d2812-c926-fbef-b191-69b6e7781c1c.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c11d2812-c926-fbef-b191-69b6e7781c1c.thumb.jpg"},{"id":"cd2cea7d-a755-2f9e-e538-0dde71649187","title":"Img2016 (3)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/cd2cea7d-a755-2f9e-e538-0dde71649187.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/cd2cea7d-a755-2f9e-e538-0dde71649187.thumb.jpg"},{"id":"6ed561a6-e2c1-6d1e-899a-65c1d1a4c40e","title":"Img2016 (2)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/6ed561a6-e2c1-6d1e-899a-65c1d1a4c40e.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/6ed561a6-e2c1-6d1e-899a-65c1d1a4c40e.thumb.jpg"},{"id":"82f05f50-876a-5f84-61e1-397bf99b61df","title":"Img2016 (1)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/82f05f50-876a-5f84-61e1-397bf99b61df.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/82f05f50-876a-5f84-61e1-397bf99b61df.thumb.jpg"},{"id":"5b06fb35-8fb9-860e-3722-32686d7c9e11","title":"Ing2016","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5b06fb35-8fb9-860e-3722-32686d7c9e11.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5b06fb35-8fb9-860e-3722-32686d7c9e11.thumb.jpg"},{"id":"ac5d56d6-6e26-a5fc-79f4-fc246532ca22","title":"Ing2016 (1)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/ac5d56d6-6e26-a5fc-79f4-fc246532ca22.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/ac5d56d6-6e26-a5fc-79f4-fc246532ca22.thumb.jpg"},{"id":"76b05f87-f5cf-54a3-7a9b-078265f710d9","title":"Ing2016 (2)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/76b05f87-f5cf-54a3-7a9b-078265f710d9.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/76b05f87-f5cf-54a3-7a9b-078265f710d9.thumb.jpg"},{"id":"7101ac13-9b87-101f-7acc-36f2428a5c9e","title":"Ing2016 (3)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/7101ac13-9b87-101f-7acc-36f2428a5c9e.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/7101ac13-9b87-101f-7acc-36f2428a5c9e.thumb.jpg"},{"id":"0cb9eed4-d14d-e0d1-0c06-44bb3fce6927","title":"Ing2016 (4)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0cb9eed4-d14d-e0d1-0c06-44bb3fce6927.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0cb9eed4-d14d-e0d1-0c06-44bb3fce6927.thumb.jpg"},{"id":"9d13d374-6c23-f3bc-e672-90d8bbb5f081","title":"Ing2016 (5)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/9d13d374-6c23-f3bc-e672-90d8bbb5f081.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/9d13d374-6c23-f3bc-e672-90d8bbb5f081.thumb.jpg"},{"id":"022cbcb2-d086-efef-94df-7849b8e86b74","title":"Ing2016 (6)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/022cbcb2-d086-efef-94df-7849b8e86b74.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/022cbcb2-d086-efef-94df-7849b8e86b74.thumb.jpg"},{"id":"bff3f712-ded2-de83-f3d2-1506b9946af0","title":"Ing2016 (7)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/bff3f712-ded2-de83-f3d2-1506b9946af0.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/bff3f712-ded2-de83-f3d2-1506b9946af0.thumb.jpg"},{"id":"3a4d7046-1187-7f9e-1e49-390e2575e5ea","title":"Ing2016 (8)","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/3a4d7046-1187-7f9e-1e49-390e2575e5ea.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/3a4d7046-1187-7f9e-1e49-390e2575e5ea.thumb.jpg"},{"id":"ba2a374d-db0c-4685-49ef-2d4f4225380d","title":"26_01_2016_003_038","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/ba2a374d-db0c-4685-49ef-2d4f4225380d.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/ba2a374d-db0c-4685-49ef-2d4f4225380d.thumb.jpg"},{"id":"8e80095e-09f4-0f9e-1a75-c8a9c41e0981","title":"Retirement","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/8e80095e-09f4-0f9e-1a75-c8a9c41e0981.png","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/8e80095e-09f4-0f9e-1a75-c8a9c41e0981.thumb.png"},{"id":"4ec03889-5316-5174-18a2-2fc70511ac34","title":"14invest6","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/4ec03889-5316-5174-18a2-2fc70511ac34.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/4ec03889-5316-5174-18a2-2fc70511ac34.thumb.jpg"},{"id":"68706f46-f84d-3a49-7976-54f508f2afa9","title":"Why LIC","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/68706f46-f84d-3a49-7976-54f508f2afa9.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/68706f46-f84d-3a49-7976-54f508f2afa9.thumb.jpg"},{"id":"5dfbe34e-f0bc-0040-daf3-ec121815416b","title":"05pension1","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5dfbe34e-f0bc-0040-daf3-ec121815416b.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5dfbe34e-f0bc-0040-daf3-ec121815416b.thumb.jpg"},{"id":"0a708748-d5ee-19b6-67da-fcada1b752b4","title":"Angry-child","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a708748-d5ee-19b6-67da-fcada1b752b4.png","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a708748-d5ee-19b6-67da-fcada1b752b4.thumb.png"},{"id":"b5023b88-d0fc-8a50-0874-e702f0ac2a21","title":"Buymediclaim","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/b5023b88-d0fc-8a50-0874-e702f0ac2a21.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/b5023b88-d0fc-8a50-0874-e702f0ac2a21.thumb.jpg"},{"id":"2911b023-20a3-eae7-ce2f-33b07f2037a9","title":"Fp-circle","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/2911b023-20a3-eae7-ce2f-33b07f2037a9.png","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/2911b023-20a3-eae7-ce2f-33b07f2037a9.thumb.png"},{"id":"5604d999-115b-44d5-5f5b-20328ddfdc33","title":"Join-lic-career","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5604d999-115b-44d5-5f5b-20328ddfdc33.gif","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/5604d999-115b-44d5-5f5b-20328ddfdc33.thumb.gif"},{"id":"0a0fdc4c-a4e7-7554-81fa-13f171657037","title":"LIC logo","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a0fdc4c-a4e7-7554-81fa-13f171657037.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a0fdc4c-a4e7-7554-81fa-13f171657037.thumb.jpg"},{"id":"0a2b3847-3eb8-178d-95cd-2d020a06c1e7","title":"Lic performance","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a2b3847-3eb8-178d-95cd-2d020a06c1e7.jpg","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/0a2b3847-3eb8-178d-95cd-2d020a06c1e7.thumb.jpg"},{"id":"80479b8d-af79-0480-192b-f460ee6fd9f0","title":"Licflag","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/80479b8d-af79-0480-192b-f460ee6fd9f0.gif","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/80479b8d-af79-0480-192b-f460ee6fd9f0.thumb.gif"},{"id":"c3378d0a-565b-fd2a-b37a-74fb9b1e62d4","title":"Nri","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c3378d0a-565b-fd2a-b37a-74fb9b1e62d4.png","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c3378d0a-565b-fd2a-b37a-74fb9b1e62d4.thumb.png"},{"id":"c1ab40f2-5d17-aa85-87bf-213614df79aa","title":"Retirement-advisor","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c1ab40f2-5d17-aa85-87bf-213614df79aa.png","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/c1ab40f2-5d17-aa85-87bf-213614df79aa.thumb.png"},{"id":"072f49b1-ad9f-8b45-396f-22886b3a7b1c","title":"Think-center","description":"","url":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/072f49b1-ad9f-8b45-396f-22886b3a7b1c.png","thumbUrl":"..\/data\/imagegallery\/eb1185e6-5be9-4b90-c7c7-7227aa3582e2\/072f49b1-ad9f-8b45-396f-22886b3a7b1c.thumb.png"}]}
         });
         });
      </script>
   </head>
   <body id="template" class="background-custom">
      <div class="site-frame">
         <div id="wrapper" class="container-content external-border3-black ">
            <!--<div class="external-top"><div><div><div><div></div></div></div></div></div>-->
            <div class="external-side-left">
               <div class="external-side-left-top">
                  <div class="external-side-left-bottom">
                     <div class="external-side-right">
                        <div class="external-side-right-top">
                           <div class="external-side-right-bottom">
                              <div id="watermark" class="pageContentText background23-white background border-none">
                                 <div id="watermark-content" class="container-content">
                                    <div id="layout">
                                       <div id="header" class="header border-none">
                                          <div id="header-top" class="top">
                                             <div>
                                                <div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="header-side" class="side">
                                             <div id="header-side2" class="side2">
                                                <div class="container-content">
                                                   <div id="header-content">
                                                      <div class="container-content-inner" id="header-content-inner">
                                                         <!--<div class="widget widget-text" id="widget-fdc8ed2e-80aa-dad9-32a1-1f9e25f1a2ee">
                                                            <div class="widget-content"><p><img id="mce-6084" style="margin-right: auto; margin-left: auto; display: block;" src="attachments/Image/topheader_1.png" alt="" width="1024" height="10" /></p></div>
                                                            </div>-->
                                                         <div class="widget widget-header" id="widget-9f0de22c-309d-01e2-c041-eb8e0648ccc6">
                                                            <div class="widget-content">
                                                               <a href="home"><img src="attachments/Header/finallogo.png" class="header-image5" alt="" style="width:220px;height:94px;"></a>
                                                               <img src="http://staging.isiwal.com/licadmincrm/admin/assets/img/profile/<?php echo isset($profile['image'])?$profile['image']:"image"; ?>" class="header-image1" alt="" style="width:140px;height:110px;">
                                                            <div class="header-image2">
                                                               <b> <?php echo isset($profile['heading'])?$profile['heading']:"heading"; ?></b>
                                                               <p> <?php echo isset($profile['name'])?$profile['name']:"name"; ?> <br> Mobile no. <?php echo isset($profile['mobile'])?$profile['mobile']:"mobile"; ?></p>
                                                               </div>
									 <div class="greetings">
														                            	<div class="blog_sid_content">
                                                               <b> <?php echo isset($profile1['wishes'])?$profile1['wishes']:"wishes"; ?></b>
                                                               <p> <?php echo isset($profile1['name'])?$profile1['name']:"name"; ?></p>
                                                              <a href="greetingspage.php"><button type="button" style="cursor:pointer; background:white;">Click Me!</button></a>
															   
                          </div>
                                                             </div> 
                                                            </div>
                                                         </div>
                                                         <div class="widget widget-navigation" id="widget-617ca861-db53-45a1-27f0-a036f4a59f85">
                                                         <div class="widget-content">
                                                            <ul class="navigation" id="navigation-617ca861-db53-45a1-27f0-a036f4a59f85">
                                                               <li class="selected">
                                                                  <a href="home">
                                                                  <span class="navigation-item-bullet">&gt;</span>
                                                                  <span class="navigation-item-text">Home</span>
                                                                  </a>
                                                               </li>
                                                               <li class=" navigation-item-expand">
                                                                  <a>
                                                                  <span class="navigation-item-bullet">&gt;</span>
                                                                  <span class="navigation-item-text">About</span>
                                                                  </a>
                                                                  <ul>
                                                                     <li class="">
                                                                        <a href="attachments/gscop.pdf" target="_blank">
                                                                        <span class="navigation-item-bullet">&gt;</span>
                                                                        <span class="navigation-item-text">About GSCOP</span>
                                                                        </a>
                                                                     </li>
                                                                     <li class="">
                                                                        <a href="about-us">
                                                                        <span class="navigation-item-bullet">&gt;</span>
                                                                        <span class="navigation-item-text">About us</span>
                                                                        </a>
                                                                     </li>
                                                                    
                                                                   
                                                                  </ul>
                                                               </li>
                                                              
                                                              
                                                              
                                                              
                                                               <li class=" navigation-item-expand">
                                                                  <a href="#">
                                                                  <span class="navigation-item-bullet">&gt;</span>
                                                                  <span class="navigation-item-text">Services</span>
                                                                  </a>
                                                                   <ul>
                                                                     <li class="">
                                                                        <a href="#">
                                                                        <span class="navigation-item-bullet">&gt;</span>
                                                                        <span class="navigation-item-text">HANDLING OF INWARD CLEARING</span>
                                                                        </a>
                                                                     </li>
                                                                     <li class="">
                                                                        <a href="#">
                                                                        <span class="navigation-item-bullet">&gt;</span>
                                                                        <span class="navigation-item-text">CAPTURE IMAGE OF CTS CHQUES</span>
                                                                        </a>
                                                                     </li>
                                                                     <li class="">
                                                                        <a href="#">
                                                                        <span class="navigation-item-bullet">&gt;</span>
                                                                        <span class="navigation-item-text">HANDLING OF INT./DIVIDEND WARRANTS.</span>
                                                                        </a>
                                                                     </li>
                                                                     <li class="">
                                                                        <a href="#">
                                                                        <span class="navigation-item-bullet">&gt;</span>
                                                                        <span class="navigation-item-text">PDC MANAGEMENT.(POST DATED CHEQUES)</span>
                                                                        </a>
                                                                     </li>
                                                                     <li class="">
                                                                        <a href="#">
                                                                        <span class="navigation-item-bullet">&gt;</span>
                                                                        <span class="navigation-item-text">HANDLING OF OUTWARD CLEARING.(CTS)</span>
                                                                        </a>
                                                                     </li>
                                                                     <li class="">
                                                                        <a href="#">
                                                                        <span class="navigation-item-bullet">&gt;</span>
                                                                        <span class="navigation-item-text">RECONCILATION OF CORPORATE ACCOUNTS</span>
                                                                        </a>
                                                                     </li>
                                                                     <li class="">
                                                                        <a href="#">
                                                                        <span class="navigation-item-bullet">&gt;</span>
                                                                        <span class="navigation-item-text">BUSINESS PROCESSING OUTSOURCE.</span>
                                                                        </a>
                                                                     </li>
                                                                     <li class="">
                                                                        <a href="#">
                                                                        <span class="navigation-item-bullet">&gt;</span>
                                                                        <span class="navigation-item-text">COLLECT RAW DATA FROM CLIENT</span>
                                                                        </a>
                                                                     </li>
                                                                     <li class="">
                                                                        <a href="#">
                                                                        <span class="navigation-item-bullet">&gt;</span>
                                                                        <span class="navigation-item-text">PROVIDING PEOPLE FROM HELP DESK TO TASK MANAGER</span>
                                                                        </a>
                                                                     </li>
                                                                      <li class="">
                                                                        <a href="#">
                                                                        <span class="navigation-item-bullet">&gt;</span>
                                                                        <span class="navigation-item-text">SCANNING OF RECORDS AND DETAIL</span>
                                                                        </a>
                                                                     </li>
                                                                  </ul>
                                                               </li>
                                                               <li class=" navigation-item-expand">
                                                                  <a href="#" >
                                                                  <span class="navigation-item-bullet">&gt;</span>
                                                                  <span class="navigation-item-text">Utilities</span>
                                                                  </a>
                                                                  <ul>
                                                                     <li class="">
                                                                        <a href="attachments/gscop.pdf" target="_blank">
                                                                        <span class="navigation-item-bullet">&gt;</span>
                                                                        <span class="navigation-item-text">About GS Cop</span>
                                                                        </a>
                                                                     </li>
                                                                     <li class="">
                                                                        <a href="our-product">
                                                                        <span class="navigation-item-bullet">&gt;</span>
                                                                        <span class="navigation-item-text">Products</span>
                                                                        </a>
                                                                     </li>
                                                                  </ul>
                                                               </li>
                                                               <li class="">
                                                                  <a href="achievements">
                                                                  <span class="navigation-item-bullet">&gt;</span>
                                                                  <span class="navigation-item-text">Achievements</span>
                                                                  </a>
                                                               </li>
                                                               <li class="">
                                                                  <a href="contact">
                                                                  <span class="navigation-item-bullet">&gt;</span>
                                                                  <span class="navigation-item-text">Contact Us</span>
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="header-bottom" class="bottom">
                                             <div>
                                                <div></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div id="columns">
                                          <div id="column2" class="column2 column column-right border-none">
                                             <div id="column2-top" class="top">
                                                <div>
                                                   <div></div>
                                                </div>
                                             </div>
                                             <div id="column2-side" class="side">
                                                <div id="column2-side2" class="side2">
                                                   <div class="container-content">
                                                      <div id="column2-content">
                                                         <div class="container-content-inner" id="column2-content-inner">
                                                            
                                                            <!--<div id="widget-3e5c4e66-2826-ed09-0e62-1cdc2a5a1385" class="widget widget-sharethis">
                                                               <h2 class="widget-title">Social Share...</h2>
                                                               <div class="widget-content">
                                                                 <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                                                               <a class="addthis_button_facebook"></a>
                                                               <a class="addthis_button_twitter"></a>
                                                               <a class="addthis_button_preferred_1"></a>
                                                               <a class="addthis_button_preferred_2"></a>
                                                               <a class="addthis_button_compact"></a>
                                                               </div></div></div>-->
                                                            
                                                            <div id="widget-6bf1c9ee-9244-65b9-200d-56fb1e35f650" class="widget widget-pagecontent">
                                                               <div class="widget-content">
                                                                  <div class="widget widget-text" id="widget-86eb1559-5207-22c0-5900-936f6611a172">
                                                                     <div class="widget-content">
                                                                        <p style="text-align: center;"><span style="font-weight: bold; font-size: 15px;"><a class="link" name="forform"></a>QUERY SUBMISSION</span></p>
                                                                     </div>
                                                                  </div>
                                                                  <div id="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed" class="widget widget-contact">
                                                                     <div class="widget-content">
                                                                        <form id="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-form" class="ajax-form2" action="php/querymail.php" method="post">
                                                                           <div class="form-item"><label class="form-item-label" for="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-name">Name <span class="form-required">*</span></label>
                                                                              <input type="text" id="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-name" name="name" class="form-item-textfield required" required>
                                                                           </div>
                                                                           <div class="form-item"><label class="form-item-label" for="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-field1">Mobile <span class="form-required">*</span></label>
                                                                              <input type="text" id="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-field1" name="field1" class="form-item-textfield required" required>
                                                                           </div>
                                                                           <div class="form-item"><label class="form-item-label" for="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-field2">Your Address</label>
                                                                              <input type="text" id="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-field2" name="field2" class="form-item-textfield" required>
                                                                           </div>
                                                                           <div class="form-item"><label class="form-item-label" for="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-mail">E-mail <span class="form-required">*</span></label>
                                                                              <input type="text" id="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-mail" name="mail" class="form-item-textfield required email" required>
                                                                           </div>
                                                                           <div class="form-item"><label class="form-item-label" for="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-message">Your Query <span class="form-required">*</span></label><textarea id="widget-6e74d978-93da-f48d-1f82-3e82bd8d18ed-field-message" name="message" class="form-item-textarea expand100-200 required"></textarea>
                                                                           </div>
                                                                           <div class="form-note">Fields marked with <span class="form-required">*</span> are required.</div>
                                                                           <input type="submit" value="Send e-mail">
                                                                        </form>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            
                                                            <!--<div id="widget-eb1185e6-5be9-4b90-c7c7-7227aa3582e2" class="widget widget-imagegallery"><h2 class="widget-title">Must View...</h2>
                                                               <div class="widget-content"><noscript>
                                                                 <div class="noscript-warning">
                                                               	<div class="noscript-warning-inner">
                                                               		<div class="noscript-warning-icon">JavaScript support is not enabled in your browser. To see the contents of this page, you need to enable JavaScript. Click <a href='http://www.google.com/support/bin/answer.py?answer=23852' target='_blank'>here</a> to learn how to enable JavaScript.</div>
                                                               	</div>
                                                               </div></noscript></div></div>-->
                                                            <!--<div class="widget widget-text" id="widget-7bed8a8f-5fef-443f-80ba-9a977f3e4146">
                                                               <div class="widget-content"><p><a class=" link" href="../contact-us/"><img id="mce-51739" style="float: left; margin-right: 20px;" src="attachments/Image/fix-appointment.jpg" alt="" width="180" height="64" /></a></p>
                                                               <p>&nbsp;</p>
                                                               <hr /></div>
                                                               </div>-->
                                                            <!--<div class="widget widget-text" id="widget-b51ca795-8e8a-4a55-a998-ad61f982491f">
                                                               <div class="widget-content"><p style="text-align: center;"><a class=" link" href="../about-us/our-vision/"><span style="font-weight: bold;">Why Consider Us</span></a></p>
                                                               <p style="text-align: center;"><a class=" link" href="../contact-us/"><img id="mce-7485" style="margin-left: auto; margin-right: auto; display: block;" src="attachments/Image/financial-advisor-for-you.gif" alt="" width="53" height="100" /></a></p>
                                                               <hr /></div>
                                                               </div>-->
                                                            <div id="widget-6bf1c9ee-9244-65b9-200d-56fb1e35f650" class="widget widget-pagecontent">
                                                               <div class="widget-content">
                                                                  <div class="widget widget-text" id="widget-9150510b-868e-8db3-adef-6c7d04bd6d82">
                                                                     <div class="widget-content"></div>
                                                                  </div>
                                                                  <!--<div class="widget widget-script" id="widget-bce3336f-3bcf-db24-6712-f3226ae36a9d">
                                                                     <div class="widget-content"
                                                                     <script language=JavaScript>
                                                                     var message="Sorry! Right Click Disabled";
                                                                     function clickIE4(){
                                                                     if (event.button==2){
                                                                     alert(message);
                                                                     return false;
                                                                     }}
                                                                     function clickNS4(e){
                                                                     if (document.layers||document.getElementById&&!document.all){
                                                                     if (e.which==2||e.which==3){
                                                                     alert(message);
                                                                     return false;
                                                                     }}}
                                                                     if (document.layers){
                                                                     document.captureEvents(Event.MOUSEDOWN);
                                                                     document.onmousedown=clickNS4;
                                                                     }
                                                                     else if (document.all&&!document.getElementById){
                                                                     document.onmousedown=clickIE4;
                                                                     }
                                                                     document.oncontextmenu=new Function("alert(message);return false")
                                                                     </script>
                                                                     </div>
                                                                     </div>-->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div id="column2-bottom" class="bottom">
                                                <div>
                                                   <div></div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="content" class="content border-none">
                                             <div id="content-top" class="top">
                                                <div>
                                                   <div></div>
                                                </div>
                                             </div>
                                             <div id="content-side" class="side">
                                                <div id="content-side2" class="side2">
                                                   <div class="container-content">
                                                      <div id="content-content">
                                                         <div class="container-content-inner" id="content-content-inner">
                                                            <div class="widget widget-breadcrumbs" id="widget-8685e8f3-5244-eb19-e021-18a5ed6b824a">
                                                               <div class="widget-content">
                                                                  <a class="icon-with-title" href="../"></a>
                                                                  <div itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
                                                                     <a class="page" href="home" itemprop="url">
                                                                     <span itemprop="title">Start</span></a><span class="separator">→</span>
                                                                  </div>
                                                                  <div itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title" class="page">About Us</span></div>
                                                               </div>
                                                            </div>
                                                            <div id="widget-b33647b2-12d5-caf1-a237-bfe08d5a9f06" class="widget widget-pagecontent">
                                                               <div class="widget-content">
                                                                  <table class="widget-columns-table">
                                                                     <tr>
                                                                        <td class="widget-columns-column" style="width: 39.655%">
                                                                           <div class="widget widget-text" id="widget-e23006d3-a464-8728-683b-414897815c15">
                                                                              <div class="widget-content">
                                                                                 <p><img id="mce-6027" src="http://staging.isiwal.com/licadmincrm/admin/assets/img/profile/<?php echo isset($profile['image'])?$profile['image']:"image"; ?>" class="header-image1" alt="" style="width:330px;height:auto;margin-right: auto; margin-left: auto; display: block;"></p> </div>
                                                                           </div>
                                                                        </td>
                                                                        <td class="widget-columns-split widget-columns-column"></td>
                                                                        <td class="widget-columns-column" style="width: 60.345%">
                                                                           <div class="widget widget-text" id="widget-b16f9514-3f04-f2c3-7876-12935a50f9e1">
                                                                              <div class="widget-content">
                                                                                 <hr />
                                                                                 <center><img id="mce-17352" style="margin-right: auto; margin-left: auto; display: block;" src="attachments/Image/welcome.png" alt="" width="450" height="130" /></center>
                                                                                 <p style="text-align: justify;"><span style="color: #000000; font-size: 14px;">To serve the scientific community we have setup an exclusive service center under the name of “G S Financial & Computer Consultancy” in 1994. The organizations are established by the core professionals of IT field with the aim of providing the best solution at affordable prices. On 1st August 2003 Company was registered under company act 1956 under the name of “GS COMPUTER CONSULTANCY PVT. LTD.” GS   promise you a very efficient services its after once you take our services. Keeping in mind the customer satisfactions and long-term mutual beneficial relationship as the prime goal to stay business. For the grow is concern of the company is has started enhancing itself in Data Processing, Data Maintaining, Development of Standardized or Customized Software, Annual Maintenance (S/W), Networking, providing IT Engg. And manpower and all IT field related works for Government & Private organizations. The company has also registered with government sector, ESI & PF departments, and service tax. Our Head office at Jhandewalan Extn, New Delhi. Our company is registered under ISO/IEC 27001:2005 for Management of Information security while delivering back office support including onsite and offsite data entry, digitization, data processing and scanning, inward –outward clearing, software development and networking services to banks.</span></p>
                                                                                 
                                                                          
                                                                                 <!--<p><span style="color: #000000; font-size: 14px;">To read more about us and to know more about <strong><em>P.C.F.P.</em></strong> please <a class=" link" href="#" target="_blank"><span class=" link"><span class=" link"><span class=" link">-->
                                                                                 <!--<span style="text-decoration: underline;">click here</span>-->
                                                                                 </span></span></span></a></span></p>
                                                                                 <p style="text-align: center;">&nbsp;</p>
                                                                                 <p style="text-align: center;">&nbsp;</p>
                                                                                 <!--<center>Remember In Life</center>-->
                                                                                 <!--<center><span style="color: #00008b; font-size: medium;"><strong>L</strong></span><span style="color: #4169e1;">ove </span><span style="color: #00008b; font-size: medium;"><strong>I</strong></span><span style="color: #4169e1;">s </span><span style="color: #00008b; font-size: medium;"><strong>C</strong></span><span style="color: #4169e1;">ompulsory</span>
                                                                                    </center>-->
                                                                                 <center>
                                                                                    <hr />
                                                                                 </center>
                                                                              </div>
                                                                           </div>
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                                  <!--<div class="widget widget-text" id="widget-e326c4f0-4ddd-4c1e-9ddb-10600abde28c">
                                                                     <div class="widget-content"><p><img id="mce-6149" style="margin-right: auto; margin-left: auto; display: block;" src="attachments/Image/11695942_10205778058989333_3608911624499967929_n.jpg" alt="" width="650" height="419" /></p></div>
                                                                     </div>-->
                                                                  <!--<div class="widget widget-video" id="widget-6123fe56-3f96-0259-77b8-3bad8d1f8803">
                                                                     <div class="widget-content"
                                                                        <iframe width="420" height="315" src="https://www.youtube-nocookie.com/embed/aVAVi7FdveI?wmode=opaque&rel=0" frameborder="0" allowfullscreen>
                                                                        </iframe>
                                                                        </div>
                                                                     </div>-->
                                                               </div>
                                                            </div>
                                                            <div class="widget widget-advertisement" id="widget-955e146a-998f-7a7c-2e6d-690d08a72a9a">
                                                               <div class="widget-content">
                                                                  <SCRIPT charset="utf-8" type="text/javascript" src="http://ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Flic0f-20%2F8005%2F406d4897-f2eb-4057-a59f-3abfaf6da1da"> </SCRIPT> 
                                                                  <NOSCRIPT><A HREF="http://ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Flic0f-20%2F8005%2F406d4897-f2eb-4057-a59f-3abfaf6da1da&Operation=NoScript">Amazon.com Widgets</A></NOSCRIPT>
                                                               </div>
                                                            </div>
                                                            <!--<div class="widget widget-script" id="widget-a9b07752-923e-3c64-cfdf-461a7a1a9f1f">
                                                               <div class="widget-content">
                                                               	<script language=JavaScript> 
                                                               	var message="Function Disabled!";
                                                               	function clickIE4()
                                                               	{
                                                               		 if (event.button==2)
                                                               		 {
                                                               			  alert(message); 
                                                               			  return false; 
                                                               			} 
                                                               		} 
                                                               		function clickNS4(e)
                                                               		{ 
                                                               			if (document.layers||document.getElementById&&!document.all)
                                                               			{ 
                                                               				if (e.which==2||e.which==3){ alert(message); return false; } } } if (document.layers){ document.captureEvents(Event.MOUSEDOWN); document.onmousedown=clickNS4; } else if (document.all&&!document.getElementById){ document.onmousedown=clickIE4; } document.oncontextmenu=new Function("alert(message);return false")
                                                               	 </script>
                                                               </div>
                                                               </div>-->
                                                            <div class="widget widget-script" id="widget-28499317-295c-bb2a-ee38-4d8685ebf8c0">
                                                               <div class="widget-content">
                                                                  <script language="JavaScript1.2">
                                                                     //Disable select-text script (IE4+, NS6+)- By Andy Scott
                                                                     //Exclusive permission granted to Dynamic Drive to feature script
                                                                     //Visit http://www.dynamicdrive.com for this script
                                                                      
                                                                     function disableselect(e){
                                                                     return false
                                                                     }
                                                                      
                                                                     function reEnable(){
                                                                     return true
                                                                     }
                                                                      
                                                                     //if IE4+
                                                                     document.onselectstart=new Function ("return false")
                                                                      
                                                                     //if NS6
                                                                     if (window.sidebar){
                                                                     document.onmousedown=disableselect
                                                                     document.onclick=reEnable
                                                                     }
                                                                  </script> 
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div id="content-bottom" class="bottom">
                                                <div>
                                                   <div></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div id="footer-wrapper">
                                       <div id="footer" class="footer border-none">
                                          <div id="footer-top" class="top">
                                             <div>
                                                <div></div>
                                             </div>
                                          </div>
                                          <div id="footer-side" class="side">
                                             <div id="footer-side2" class="side2">
                                                <div class="container-content">
                                                   <div id="footer-content">
                                                      <div class="container-content-inner" id="footer-content-inner">
                                                         <div class="widget widget-text" id="widget-73fdf89a-3b66-ffa8-7965-cab511a338e3">
                                                            <div class="widget-content">
                                                               <p style="text-align: center;">&copy; 1989-2017. GS COP Ltd . All Rights Reserved.</p>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="footer-bottom" class="bottom">
                                             <div>
                                                <div></div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="external-bottom">
               <div>
                  <div>
                     <div>
                        <div></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script type="text/javascript" src="js/anti_cache.js"></script>
      <script type="text/javascript">
         var _gaq = _gaq || [];
         _gaq.push(['_setAccount', 'UA-34705881-1']);
         _gaq.push(['_trackPageview']);
         
         (function() {
           var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
           ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
           var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
         })();
         
      </script>
   </body>
</html>