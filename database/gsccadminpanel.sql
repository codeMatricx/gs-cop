-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 23, 2018 at 06:28 AM
-- Server version: 5.6.36-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gsccadminpanel`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(250) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `email` varchar(60) NOT NULL,
  `date` varchar(20) NOT NULL,
  `price` varchar(40) NOT NULL,
  `request_id` varchar(100) NOT NULL,
  `transaction_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `email`, `date`, `price`, `request_id`, `transaction_id`) VALUES
(2, 'arpit', 'arpit.intel@gmail.com', '2017-09-11', '2999.99', 'MOJO7911005A15126542', '9c3b35505641449c9c8ba2d9dc930004'),
(3, 'arpit', 'arpit.intel@gmail.com', '2017-09-11', '2999.99', 'MOJO7911005A15126542', '9c3b35505641449c9c8ba2d9dc930004'),
(4, 'arpit', 'arpit.intel@gmail.com', '2017-09-11', '2999.99', 'MOJO7911005A15126542', '9c3b35505641449c9c8ba2d9dc930004'),
(5, 'arpit', 'arpit.intel@gmail.com', '2017-09-11', '2999.99', 'MOJO7911005A15126542', '9c3b35505641449c9c8ba2d9dc930004');

-- --------------------------------------------------------

--
-- Table structure for table `greetings`
--

CREATE TABLE IF NOT EXISTS `greetings` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `wishes` varchar(200) NOT NULL,
  `name` varchar(100) NOT NULL,
  `images` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `greetings`
--

INSERT INTO `greetings` (`id`, `wishes`, `name`, `images`) VALUES
(1, 'DO GOOD BE GOOD', 'BELIEVE IN YOURSELF', 'entheos_logo.png');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=29 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `name`) VALUES
(16, '5.jpg'),
(17, '6.jpg'),
(19, '1.jpg'),
(22, 'licc.jpg'),
(24, 'mylic.png'),
(25, 'welc.jpg'),
(28, 'LIC_LOWSALARY.png');

-- --------------------------------------------------------

--
-- Table structure for table `licproduct`
--

CREATE TABLE IF NOT EXISTS `licproduct` (
  `id` int(240) NOT NULL AUTO_INCREMENT,
  `link` varchar(2000) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `licproduct`
--

INSERT INTO `licproduct` (`id`, `link`, `name`) VALUES
(4, 'https://licindia.in/Products/Insurance-Plan/Lic_Jeevan_Utkarsh', 'JEEVAN UTKARSH');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(5000) NOT NULL,
  `link` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `description`, `link`) VALUES
(3, 'About Lic', 'Life Insurance Corporation of India (LIC) is an Indian state-owned insurance group and investment company headquartered in Mumbai. It is the largest insurance company in India with an estimated asset value of â‚¹1,560,482 crore (US$240 billion).[2] As of 2013 it had total life fund of Rs.1433103.14 crore with total value of policies sold of 367.82 lakh that yearadnb shmdabvcxvmhdvjsdhfusdakhgfagvhsaghjsdbvhjsbhsdvbhdnvbhvhjsdv adnb shmdabvcxvmhdv', 'https://en.wikipedia.org/wiki/Life_Insurance_Corporation'),
(4, 'Sensex jumps nearly 200 pts.', 'Shrugging off tepid opening on disappointing GDP numbers for the June quarter, the domestic stock market continued to strengthen in afternoon session on rising hopes of another rate cut by the Reserve Bank of India.   The BSE Sensex was trading up by 185.09 points or 0.58 per cent at 31,915.58 around 02.00 pm, while NSE Nifty50 was up by 55.40 points, 0.56 per cent at 9,973.30', 'https://en.wikipedia.org/wiki/Life_Insurance_Corporation');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `details` varchar(2000) NOT NULL,
  `price` varchar(20) NOT NULL,
  `date` varchar(20) NOT NULL,
  `image` varchar(50) NOT NULL,
  `demo_product` varchar(100) DEFAULT NULL,
  `full_product` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=15 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `details`, `price`, `date`, `image`, `demo_product`, `full_product`) VALUES
(14, 'GSCOP', 'ACTIVITY RECORD @5100 4yr', '5100', '2017-11-04', 'HAPPYDIWALI1.jpg', 'HAPPYDIWALI1.jpg', 'HAPPYDIWALI1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(20) NOT NULL,
  `name` varchar(40) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `heading` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `email`, `password`, `name`, `mobile`, `heading`, `image`) VALUES
(1, 'gsfcc@bol.net.in,gsfcconline@gmail.com', '123456789', 'SANJAY SHARMA', '9891478007', 'CTS CLEARING AND SCANNING', 'user.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `id` int(240) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `clia` varchar(40) NOT NULL,
  `location` varchar(100) NOT NULL,
  `uid` varchar(40) NOT NULL,
  `contact` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `name`, `clia`, `location`, `uid`, `contact`) VALUES
(1, 'SANJAY SHARMA', 'CONSULTANT', 'GREATER NOIDA /  JHANDEWALAN / NOIDA', '86214', '9891478007'),
(2, 'Rakesh Jha', 'Marketing Head', 'Sec-16', '1234', '9650701465'),
(4, 'DEEPSHIKHA CHATERJEE', 'DEVELOPMENT OFFICER', 'JEEVAN TARA BUILDING', 'BO_0001', '01141540139 EXT 23');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
