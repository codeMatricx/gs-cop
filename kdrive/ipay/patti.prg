SET EXCL ON
set talk off
set stat off
SET DATE BRIT
SET CENT ON
STORE 0 TO M.TOT,M.NOI,M.LN
@0,0 CLEA
@ 12,1 SAY 'ENTER PATTI FILE DataBase ' GET M.PATTI DEFA "K:\PATTI\Cheque.dbf                        " VALI !EMPTY(M.PATTI)
@ 14,1 say 'WANT TO FILTERING DATA....' GET M.RESP PICT 'Y' DEFA 'N'
READ
if lastkey()=27
	retu
endif
M.PATTI=ALLT(PATTI)
USE &PATTI 
IF M.RESP='Y'
	@ 16,1 SAY 'CONDITION APPLY...........' GET COND DEFA "SUBGROUP='ABC010104'                      " VALI !EMPTY(M.COND)
	READ
	COND=ALLT(COND)
	SET FILT TO &COND
ENDIF
m.no=1
do while !eof()
	repl no with m.no
	m.no=m.no+1
	skip
enddo
INDEX ON NO TAG NO ADDITIVE
GO TOP
SET DEVI TO PRINT
SET PRINT ON
SET PRINT TO C:\FF.TXT
?' CHQNO      AMOUNT      CHQNO      AMOUNT      CHQNO      AMOUNT      CHQNO      AMOUNT'
?
M.NO=NO
DO WHILE !EOF()
	IF SEEK(M.NO)
		M.NOI=M.NOI+1
		M.TOT=M.TOT+AMOUNT
		?CHQNO,str(AMOUNT,11,2)
	ELSE
		?
		?'      No of Cheques '+allt(str(m.noi))
		?'       Total Amount '+allt(str(m.tot,14,2))
		?
		EJEC
		EXIT
	ENDIF
	IF SEEK(M.NO+50)
		M.NOI=M.NOI+1
		M.TOT=M.TOT+AMOUNT
		??SPAC(4),CHQNO,str(AMOUNT,11,2)
	ELSE
		SEEK(M.NO)
	ENDIF
	IF SEEK(M.NO+100)
		M.NOI=M.NOI+1
		M.TOT=M.TOT+AMOUNT
		??SPAC(4),CHQNO,str(AMOUNT,11,2)
	ELSE
		SEEK(M.NO)
	ENDIF
	IF SEEK(M.NO+150)
		M.NOI=M.NOI+1
		M.TOT=M.TOT+AMOUNT
		??SPAC(4),CHQNO,str(AMOUNT,11,2)
	ELSE
		SEEK(M.NO)
	ENDIF
	M.NO=M.NO+1
	M.LN=M.LN+1
	IF M.LN>=50
		?
		?'      No of Cheques '+allt(str(m.noi))
		?'       Total Amount '+allt(str(m.tot,14,2))
		?
		EJEC
		IF ! EOF()
	        ?' CHQNO      AMOUNT      CHQNO      AMOUNT      CHQNO      AMOUNT      CHQNO      AMOUNT'
			?
		ENDIF
		STORE 0 TO M.TOT,M.NOI,M.LN
		M.NO=M.NO+150
	ENDIF
ENDDO
SET DEVI TO SCREEN
SET PRINT TO
SET PRINT OFF
run edit C:\FF.TXT
CLEA ALL
RETU
