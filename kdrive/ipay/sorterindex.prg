clos data
clos all
clea all
set talk off
set safe off
set echo off
set excl on
Wait Window 'Indexing On ....' Nowait

Use data\Sorter 
pack
Dele Tag All
index on chqno										 tag Chqno
inde  on bcode+dtos(pdate)+chqno+allt(str(amt,15,2)) tag bcodechq
index on bCode+Dtos(pDate)+Imgpath					 tag bcodeimg
index on dtos(pdate)+chqno							 tag pdatechq
INDE ON BCODE+DTOS(PDATE) 					   		 TAG BCODEPDATE
inde on chqno+allt(str(amt,15,2)) 				     tag chqamt
INDEX ON DTOS(pdate)+chqno+ALLTRIM(STR(amt,15,2))	 TAG DATECHQAMT
inde  on pabank+dtos(pdate)+chqno+allt(str(amt,15,2)) tag pabankchq
INDEX ON DTOS(pdate)+ALLTRIM(lotno)+ALLTRIM(filepath) TAG sortpath

Use hvdata\Sorter 
pack
Dele Tag All
index on chqno											tag Chqno
inde  on bcode+dtos(pdate)+chqno+allt(str(amt,15,2)) 	tag bcodechq
index on bCode+Dtos(pDate)+Imgpath						tag bcodeimg
index on dtos(pdate)+chqno								tag pdatechq
INDE ON BCODE+DTOS(PDATE) 								TAG BCODEPDATE
inde on chqno+allt(str(amt,15,2)) 						tag chqamt
INDEX ON DTOS(pdate)+chqno+ALLTRIM(STR(amt,15,2))		TAG DATECHQAMT
inde  on pabank+dtos(pdate)+chqno+allt(str(amt,15,2)) tag pabankchq
INDEX ON DTOS(pdate)+ALLTRIM(lotno)+ALLTRIM(filepath) TAG sortpath

