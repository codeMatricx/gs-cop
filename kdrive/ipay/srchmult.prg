*:*****************************************************************************
*:
*: Procedure file: F:\SAFE\NPRG\SRCHMULT.PRG
*:
*:         System: 
*:         Author: 
*:      Copyright (c) 1996, 
*:  Last modified: 06/17/95     10:37
*:
*:  Procs & Fncts: SRCHMULT()
*:               : DISP_KEY
*:               : DISP_INFO
*:               : HILITE
*:
*:          Calls: ALIAS()            (function  in ?)
*:               : SA()               (function  in ?)
*:               : USED()             (function  in ?)
*:               : RECNO()            (function  in ?)
*:               : ORDER()            (function  in ?)
*:               : EMPTY()            (function  in ?)
*:               : NDX()              (function  in ?)
*:               : IIF()              (function  in ?)
*:               : SUBSTR()           (function  in ?)
*:               : RAT()              (function  in ?)
*:               : FILTER()           (function  in ?)
*:               : AT()               (function  in ?)
*:               : CHR()              (function  in ?)
*:               : ALLTRIM()          (function  in ?)
*:               : LEN()              (function  in ?)
*:               : VAL()              (function  in ?)
*:               : WROWS()            (function  in ?)
*:               : FOUND()            (function  in ?)
*:               : LEFT()             (function  in ?)
*:               : DISP_KEY           (procedure in SRCHMULT.PRG)
*:               : DISP_INFO          (procedure in SRCHMULT.PRG)
*:               : PADC()             (function  in ?)
*:               : INKEY()            (function  in ?)
*:               : HILITE             (procedure in SRCHMULT.PRG)
*:               : UPPER()            (function  in ?)
*:               : BETWEEN()          (function  in ?)
*:               : RECCOUNT()         (function  in ?)
*:
*:        Indexes: X).IDX             
*:
*:      Documented 04/03/96 at 11:42               FoxDoc  version 2.10f
*:*****************************************************************************
PARAMETERS sa
EXTERNAL ARRAY sa, ap

PRIVATE curr_dbf,curr_rec,curr_ord,curr_ndx,curr_filter
PUSH KEY
curr_dbf = ALIAS()

sn_file = sa(1)
sn_index= sa(2)
sn_tag  = (sa(3) = 0)
IF USED(sn_file)
   sn_fo = .T.
   SELECT &sn_file
   curr_rec = RECNO()
   IF sn_tag
      curr_ord = ORDER()
      SET ORDER TO (sn_index)
   ELSE
      curr_ord = ORDER()
      curr_ndx = ''
      FOR sn_i = 1 TO 10
         IF EMPTY(NDX(i))
            EXIT
         ENDIF
         temp_var = NDX(i)
         curr_ndx = IIF(EMPTY(curr_ndx),'',curr_ndx+',') + SUBSTR(temp_var,RAT('\',temp_var)+1)
      ENDFOR
      SET INDEX TO (sn_index)
   ENDIF
ELSE
   sn_fo = .F.
*  SELECT 0 && CHANGE BY RAVI
   IF sn_tag
      SELE (sn_file)
      SET ORDER TO (sn_index)
   ELSE
      SELE (sn_file)
      SET ORDER TO (sn_index)
   ENDIF
ENDIF
curr_filter = FILTER()
IF !EMPTY(sa(6))
   SET FILTER TO &sa(6)
ENDIF

IF AT(CHR(201),sa(4)) = 0
   sn_fld = sa(4)
   sn_hdr = ''
ELSE
   sn_fld = ALLTRIM(SUBSTR(sa(4),1,AT(CHR(201),sa(4))-1))
   sn_hdr = ALLTRIM(SUBSTR(sa(4),AT(CHR(201),sa(4))+1))
ENDIF
sn_fld        = sn_fld + '+ " ["' + '+ iif(' + sa(11) + "(recno()),'*',' ')" + '+"]"'
sn_sel_stmt   = sa(11) + "(recno()) = .not. " + sa(11) + "(recno())"
sn_sel_arr    = sa(11) + "(recno())"
sn_disp_width = LEN(sn_fld) + 4
*sn_disp_width = LEN(&sn_fld) + 8

IF EMPTY(sa(10))
   sn_left = 79 - sn_disp_width - 1
   DEFINE WINDOW sn_win FROM 03,sn_left TO 23,79 DOUBLE COLOR RGB(,,,128,128,128)
ELSE
   sn_wl = VAL(SUBSTR(sa(10),AT(',',sa(10))+1))
   sn_wt = VAL(SUBSTR(sa(10),AT(',',sa(10))-1))
   sn_wr = sn_wl + sn_disp_width + 1
   DEFINE WINDOW sn_win FROM sn_wt,sn_wl TO 23,sn_wr DOUBLE COLOR RGB(,,,128,128,128)
ENDIF

* -/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-
sn_esc = 27                                               && escape
sn_ret = 13                                               && return
sn_bks = 127                                              && backspace
sn_lar = 19                                               && left arrow
sn_dar = 24                                               && down arrow
sn_uar = 5                                                && up arrow
sn_pgd = 3                                                && page down
sn_pgu = 18                                               && page up
sn_top = 1                                                && top of screen
sn_bot = 6                                                && bottom of screen
sn_ins = 22                                               && insert (add)
sn_end = 23                                               && ctrl + end
* -/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-

DECLARE sn_command(10) , sn_msg(10)

STORE 0 TO sn_cj , sn_cm
IF sa(12) > 0
   FOR sn_i = 1 TO sa(12)
      IF AT('/',sa(12+sn_i)) = 0
         sn_cj = sn_cj + 1
         sn_command(sn_cj) = sa(12+sn_i)
      ELSE
         temp_var         = SUBSTR(sa(12+sn_i),1,AT('/',sa(12+sn_i))-1)
         sn_cj            = sn_cj + 1
         sn_command(sn_cj)= temp_var
         temp_var         = SUBSTR(sa(12+sn_i),AT('/',sa(12+sn_i))+1)
         sn_cm            = sn_cm + 1
         sn_msg(sn_cm)    = temp_var
      ENDIF
   ENDFOR
ENDIF
FOR sn_i = 1 TO sn_cj
   &sn_command(sn_i)
ENDFOR

temp_var = sn_cm
sn_rows = WROWS('sn_win') - temp_var - 6
sn_lrow = WROWS('sn_win') - 1

* -/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-
sn_c_top = 0
sn_c_bot = 0
sn_c_ptr = 0
sn_cnt   = 0
sn_i     = 0
* -/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-

ACTIVATE WINDOW sn_win

DIMENSION sn_buff(sn_rows+3)                              && first display is at row 3
sn_key = sa(5) + sa(9)
DO WHILE LEN(sn_key) <> 0
   SEEK sn_key
   IF FOUND()
      EXIT
   ENDIF
   sn_key = LEFT(sn_key,LEN(sn_key)-1)
ENDDO                WHILE LEN(sn_key) <> 0
IF LEN(sn_key) = 0
   GO TOP
ENDIF
DO disp_key
DO disp_info

DO WHILE .T.
   @ 0,0 SAY PADC(sn_key,sn_disp_width) COLOR 0/7

   SET CURSOR OFF
   ch = 0
   DO WHILE ch = 0
      ch = INKEY()
   ENDDO                                   WHILE ch = 0
   SET CURSOR ON

   DO CASE
      CASE ch = sn_esc
         ans_key = -1
         deac wind sn_win
         rele wind sn_win
         EXIT
*        retu TO Master

      CASE ch = sn_end
         ans_key = 0
         EXIT

      CASE ch = sn_ret
         IF &sn_sel_arr
            &sn_sel_arr = .F.
         ELSE
            &sn_sel_arr = .T.
         ENDIF
         sn_old_ptr = sn_c_ptr
         sn_c_ptr   = sn_c_ptr
         DO hilite
         sn_key = ''

      CASE ch = sn_bks .OR. ch = sn_lar
         IF LEN(sn_key) > LEN(sa(5)) + 1
            sn_key = LEFT(sn_key,LEN(sn_key)-1)
         ELSE
            sn_key = sa(5)
         ENDIF
         IF LEN(sn_key) <> 0
            SEEK sn_key
         ELSE
            GO TOP
         ENDIF
         sn_shown = .F.
         FOR sn_i = sn_c_top TO sn_c_bot
            IF RECNO() = sn_buff(sn_i)
               sn_shown = .T.
               EXIT
            ENDIF
         ENDFOR
         IF sn_shown
            sn_old_ptr = sn_c_ptr
            sn_c_ptr = sn_i
            DO hilite
         ELSE
            DO disp_key
         ENDIF

      CASE ch = sn_dar
         sn_old_ptr = sn_c_ptr
         sn_c_ptr = sn_c_ptr+1
         DO hilite
         sn_key = sa(5)

      CASE ch = sn_uar
         sn_old_ptr = sn_c_ptr
         sn_c_ptr = sn_c_ptr-1
         DO hilite
         sn_key = sa(5)

      CASE ch = sn_top
         sn_old_ptr = sn_c_ptr
         sn_c_ptr = sn_c_top
         DO hilite
         sn_key = sa(5)

      CASE ch = sn_bot
         sn_old_ptr = sn_c_ptr
         sn_c_ptr = sn_c_bot
         DO hilite
         sn_key = sa(5)

      CASE ch = sn_pgd
         sn_old_ptr = sn_c_ptr
         sn_c_ptr   = sn_c_bot + 1
         DO hilite
         sn_key = sa(5)

      CASE ch = sn_pgu
         sn_old_ptr = sn_c_ptr
         sn_c_ptr   = sn_c_top-sn_rows-1
         DO hilite
         sn_key = sa(5)

      CASE ch >= 32 .AND. ch <= 125
         sn_key = sn_key + UPPER(CHR(ch))
         sn_rec = RECNO()
         IF SEEK (sn_key)
            sn_shown = .F.
            FOR sn_i = sn_c_top TO sn_c_bot
               IF RECNO() = sn_buff(sn_i)
                  sn_shown = .T.
                  EXIT
               ENDIF
            ENDFOR
            IF sn_shown
               sn_old_ptr = sn_c_ptr
               sn_c_ptr = sn_i
               DO hilite
            ELSE
               DO disp_key
            ENDIF
         ELSE
            ?CHR(7)
            ?CHR(7)
            WAIT WINDOW "RECORD NOT FOUND....." NOWAIT
            sn_key = LEFT(sn_key,LEN(sn_key)-1)
            GOTO sn_rec
         ENDIF
   ENDCASE
ENDDO                                   WHILE .T.

DEACTIVATE WINDOW sn_win
RELEASE WINDOW sn_win

IF sn_fo
   IF sn_tag
      IF EMPTY(curr_ord)
         SET ORDER TO
      ELSE
         SET ORDER TO (curr_ord)
      ENDIF
   ELSE
      IF EMPTY(curr_ndx)
         SET INDEX TO
      ELSE
         SET INDEX TO (curr_ndx)
         IF EMPTY(curr_ord)
            SET ORDER TO
         ELSE
            SET ORDER TO (curr_ord)
         ENDIF
      ENDIF
   ENDIF
   IF !EMPTY(curr_filter)
      SET FILTER TO &curr_filter
   ELSE
      SET FILTER TO
   ENDIF
   IF BETWEEN(curr_rec,1,RECCOUNT())
      GOTO curr_rec
   ENDIF
ELSE
   SELECT &sn_file
*   USE -- change by ravi
ENDIF

IF !EMPTY(curr_dbf)
   SELECT (curr_dbf)
ENDIF
POP KEY
RETURN ans_key

*******************
PROCEDURE disp_key
*******************
   parameter REFRESH
   IF PARAMETERS() < 1
      REFRESH = .F.
   ENDIF
   IF REFRESH
      DO disp_info
   ENDIF
   @ 2,0 CLEAR TO sn_rows+2,sn_disp_width
   sn_color = '0/7'
   sn_c_top = 2
   DO CASE
      CASE LEN(sn_key) <> 0
         FOR sn_i = sn_c_top TO sn_c_top + sn_rows
            @ sn_i,1 SAY &sn_fld COLOR &sn_color
            sn_buff(sn_i) = RECNO()
            sn_c_bot = sn_i
            sn_color = '7/0'
            SKIP
            IF EOF()
               EXIT
            ENDIF
         ENDFOR
      OTHERWISE
         IF !EOF()
            FOR sn_i = sn_c_top TO sn_c_top + sn_rows
               @ sn_i,1 SAY &sn_fld COLOR &sn_color
               sn_buff(sn_i) = RECNO()
               sn_c_bot = sn_i
               sn_color = '7/0'
               SKIP
               IF EOF()
                  EXIT
               ENDIF
            ENDFOR
         ELSE
            sn_c_bot = 0
         ENDIF
   ENDCASE
   sn_c_ptr = sn_c_top
   IF sn_c_bot > 0
      GOTO sn_buff(sn_c_ptr)
   ENDIF
   RETURN
   
*******************
PROCEDURE hilite
*******************
IF sn_c_bot <= 0
   RETURN
ENDIF
IF sn_c_ptr >= sn_c_top .AND. sn_c_ptr <= sn_c_bot
   IF sn_old_ptr >= sn_c_top .AND. sn_old_ptr <= sn_c_bot
      GOTO sn_buff(sn_old_ptr)
      @ sn_old_ptr,1 SAY &sn_fld COLOR 7/0
   ENDIF
   GOTO sn_buff(sn_c_ptr)
   @ sn_c_ptr,1 SAY &sn_fld COLOR 0/7
   RETURN
ENDIF
GOTO sn_buff(sn_old_ptr)
IF sn_c_ptr < sn_c_top
   FOR sn_i = sn_old_ptr-1 TO sn_c_ptr STEP -1
       SKIP -1
       IF BOF()
          ?CHR(7)
          ?CHR(7)
          WAIT WINDOW "ALREADY TOP OF THE FILE...." NOWAIT
          GO TOP
          EXIT
       ENDIF
   ENDFOR
   DO disp_key
ELSE
   IF sn_c_ptr > sn_c_bot
      FOR sn_i = sn_old_ptr+1 TO sn_c_ptr
          SKIP
          IF EOF()
             ?CHR(7)
             ?CHR(7)
             WAIT WINDOW "ALREADY END OF THE FILE...." NOWAIT
             GO BOTTOM
             EXIT
          ENDIF
      ENDFOR
      DO disp_key
   ENDIF
ENDIF
RETURN

*******************
PROCEDURE unsel
*******************
   PUSH KEY CLEAR
   FOR si = 1 TO no_of_prty
      ap(si) = .F.
   ENDFOR
   DO disp_key WITH .T.
   POP KEY
   RETURN

*******************
PROCEDURE sel
*******************
   PUSH KEY CLEAR
   FOR si = 1 TO no_of_prty
      ap(si) = .T.
   ENDFOR
   DO disp_key WITH .T.
   POP KEY
   RETURN

*******************
PROCEDURE disp_info
*******************
   sn_drow = sn_lrow
   @ sn_drow,0 SAY PADC('<ESC>-Cancel / <Ctrl>+<End>-Proceed',sn_disp_width) COLOR 0/7
   sn_drow = sn_drow - 1
   @ sn_drow,0 SAY PADC('<RET> to Select / Deselect',sn_disp_width) COLOR 0/7
   sn_drow = sn_drow - 1
   FOR sn_i = 1 TO sn_cm
      @ sn_drow,0 SAY PADC(sn_msg(sn_i),sn_disp_width)
      sn_drow = sn_drow - 1
   ENDFOR
   @ sn_drow,0 TO sn_drow,sn_disp_width
   IF EMPTY(sn_hdr)
      @ 1,0 TO 1,sn_disp_width
   ELSE
      @ 1,1 SAY sn_hdr
   ENDIF
   RETURN

* ---------------------------------------------------------------------
  proc sel_ch
* ---------------------------------------------------------------------
PUSH KEY clea
*ww = WONTOP()
*SHOW wind &ww
*acti scre

define wind ww1 from 20,05 to 22,40
acti wind ww1
ach = spac(len(sn_fld))
SET curs ON
*@23,02 clea TO 23,30
@00,02 SAY 'Enter Choice : ' GET ach pict '@!'
READ
SET curs OFF
FOR si = 1 TO no_of_prty
    GO si
* --- Change By Ravi    Date 20/01/1999
*   IF subs(&sa(4),1,1) = ach
    IF subs(&sa(4),1,LEN(ALLT(ach))) = ALLT(ach)
       ap(si) = .T.
    ENDIF
ENDFOR
deac wind ww1
rele wind ww1
DO disp_key WITH .T.
POP KEY
RETURN

* ---------------------------------------------------------------------
 proc unsel_ch
* ---------------------------------------------------------------------
PUSH KEY clea
ww = WONTOP()
SHOW wind &ww
acti scre
*ach = spac(1)
ach = spac(len(sn_fld))

SET curs ON
@24,02 clea TO 24,30
@24,02 SAY 'Enter Choice : ' GET ach pict '@!'
READ
acti wind &ww
SET curs OFF

FOR si = 1 TO no_of_prty
    GO si
* --- Change By Ravi    Date 20/01/1999
*   IF subs(&sa(4),1,1) = ach
    IF subs(&sa(4),1,LEN(ALLT(ach))) = ALLT(ach)
       ap(si) = .F.
    ENDIF
ENDFOR
DO disp_key WITH .T.
POP KEY
RETURN
