clos data
clos all
clea all
set talk off
set safe off
set echo off
set excl on
Wait Window 'Indexing On ....' Nowait

use data\setup
pack
dele tag all
inde on desc tag desc

use data\tcmast
pack
dele tag all
inde on tccode 								tag tccode

Use data\Drawmick
pack
Dele Tag all
index on chqno								tag chqno
index on seqno								tag seqno
index on chqno+seqno						tag ChqSeq
index on pBank+Dtos(pDate)+chqno+seqno		tag pbankchq
index on pBank+Dtos(pDate)+chqno			tag pbankdtchq
index on pBank+Dtos(pDate)+seqno			tag pbankseq
index on dBank+Chqno						tag Dbankchq
index on warseq								tag warseq desc
index on pBank+dtos(pdate)					tag Bankdate
index on pBank+dtos(pdate)+Mark				tag pbankdate
index on bCode+Chqno						tag bcodechq
index on Dtos(pDate)						tag pDate
Index On bCode+dtos(pDate)					tag bankpd
index on Right(pBank,3)+Dtos(pDate)+Chqno	tag BankCod
index on Dtos(pDate)+Chqno					tag pdateChq
index on pBank+dtos(pdate)+tccode			tag Bankdatetc

Use data\Sorter 
PACK

Dele Tag All
index on chqno										  			 tag Chqno
inde  on bcode+dtos(pdate)+chqno+allt(str(amt,15,2))  			 tag bcodechq
index on bCode+Dtos(pDate)+Imgpath					  			 tag bcodeimg
index on dtos(pdate)+chqno							  			 tag pdatechq
INDE ON BCODE+DTOS(PDATE) 					   		  			 TAG BCODEPDATE
inde on chqno+allt(str(amt,15,2)) 				      			 tag chqamt
INDEX ON DTOS(pdate)+chqno+ALLTRIM(STR(amt,15,2))	  			 TAG DATECHQAMT
inde  on pabank+dtos(pdate)+chqno+allt(str(amt,15,2)) 			 tag pabankchq
INDEX ON DTOS(pdate)+ALLTRIM(lotno)+ALLTRIM(filepath) 			 TAG sortpath
INDEX ON recid 									  	        	 TAG recid
INDEX on DTOS(pdate)+chqno+ALLTRIM(STR(amt,15,2))+ALLTRIM(lotno) TAG pchqamtlot
inde  on dtos(pdate)								  			 tag pdate
 
Use hvdata\Sorter 
pack
Dele Tag All
index on chqno											tag Chqno
inde  on bcode+dtos(pdate)+chqno+allt(str(amt,15,2)) 	tag bcodechq
index on bCode+Dtos(pDate)+Imgpath						tag bcodeimg
index on dtos(pdate)+chqno								tag pdatechq
INDE ON BCODE+DTOS(PDATE) 								TAG BCODEPDATE
inde on chqno+allt(str(amt,15,2)) 						tag chqamt
INDEX ON DTOS(pdate)+chqno+ALLTRIM(STR(amt,15,2))		TAG DATECHQAMT
inde  on pabank+dtos(pdate)+chqno+allt(str(amt,15,2))  tag pabankchq
INDEX ON DTOS(pdate)+ALLTRIM(lotno)+ALLTRIM(filepath)  TAG sortpath
INDEX on recid 											TAG recid
INDEX on DTOS(pdate)+chqno+ALLTRIM(STR(amt,15,2))+ALLTRIM(lotno) TAG pchqamtlot

Use data\Payee Excl
pack
Dele Tag all
INDEX on recid 											TAG recid
index on ddate								tag ddate Desc
index on chqno								tag chqno
index on chqno+seq							tag chqseq
index on bCode+Dtos(pdate)+Code				tag bankcode
index on Dtos(pdate)+payee					tag pDate
index on bCode+Dtos(pDate)+Chqno			tag bcodechq
index on bCode+Dtos(pdate)+Code+chqno		tag bankcdchq
index on dtos(pdate)+chqno					tag pdatechq
index on bcode+DTOS(pdate)+code+chqno+ALLTRIM(STR(amount,15,2))	tag bankcdchq1
index on bcode+DTOS(pdate)+code+PADL(ALLTRIM(seq),6," ")		tag bankcdseq
INDEX ON DTOS(pdate)+chqno+ALLTRIM(STR(amount,15,2)) 			tag CHEQUEAMT


Use data\pMast Excl
pack
Dele Tag all
index on pName								tag pName
index on seq								tag seq

Use data\retu1 Excl
pack
Dele Tag all
index on sod								tag sod
index on reason								tag reason
Use data\rights Excl
pack
Dele Tag all
index on fld1+fld2							tag unq
index on fld1+Fld2							tag upName
index on fld1+Fld2+Fld3						tag uBank
index on fld1+Fld2+Fld3+fld4				tag uBank1

Use data\uMast Excl
pack
Dele Tag all
index on uCode								Tag uCode Desc
index on Fld1								Tag Fld1

Use data\Warr_mast Excl
pack
Dele Tag all
Index on Warr_Acno							Tag Warr_Acno

Use data\Implog Excl
pack
Delete Tag all
index on Alltrim(fName)+Dtos(pdate)+Bcode+suffm	Tag implog


Use data\Brnmst Excl
pack
Delete Tag all
Index on Brname  Tag Brname
Index on Pa_bank Tag Pa_bank
Index on brCode  Tag brCode

Use data\Branch Excl
pack
Delete Tag All
Inde on		Bkcd 				Tag Bkcd
inde on 	substr(bkcd,4,3) 	tag bankcode
inde on		rest				tag bankname

Use data\helpa Excl
pack
Delete Tag All
inde on ucode+key_name tag usrkey

use data\namemast excl
pack
dele tag all
inde on code 							tag code
inde on name 							tag name

use data\bissue excl
pack
dele tag all
inde on issue_code				tag issue_code
inde on name 					tag name

use data\bonds excl
pack
dele tag all
inde on issue_code+bankcd+brl+ucode+smono	tag smono
inde on issue_code+bankcd+brl+ucode			tag issueusr
inde on bankcd+appno+appno1					tag appno
inde on issue_code+bankcd+brl+smono					tag banksmono
inde on issue_code+bankcd+brl+dtos(date)+smono		tag bankdate
inde on chqbank+chqno						tag chqno

use data\broker excl
pack 
dele tag all
inde on brokercode							tag brokercode
inde on brokername							tag brokername

clos data


*-HIGH VALUE INDEXING

Use hvdata\Drawmick
pack
Dele Tag all
index on chqno								tag chqno
index on seqno								tag seqno
index on chqno+seqno						tag ChqSeq
index on pBank+Dtos(pDate)+chqno+seqno		tag pbankchq
index on pBank+Dtos(pDate)+chqno			tag pbankdtchq
index on pBank+Dtos(pDate)+seqno			tag pbankseq
index on dBank+Chqno						tag Dbankchq
index on warseq								tag warseq desc
index on pBank+dtos(pdate)					tag Bankdate
index on pBank+dtos(pdate)+Mark				tag pbankdate
index on bCode+Chqno						tag bcodechq
index on Dtos(pDate)						tag pDate
Index On bCode+dtos(pDate)					tag bankpd
index on Right(pBank,3)+Dtos(pDate)+Chqno	tag BankCod
index on Dtos(pDate)+Chqno					tag pdateChq
index on pBank+dtos(pdate)+tccode			tag Bankdatetc

Use hvdata\Sorter 
pack
Dele Tag All
index on chqno											tag Chqno
inde  on bcode+dtos(pdate)+chqno+allt(str(amt,15,2)) 	tag bcodechq
index on bCode+Dtos(pDate)+Imgpath						tag bcodeimg
index on dtos(pdate)+chqno								tag pdatechq
INDE ON BCODE+DTOS(PDATE) 								TAG BCODEPDATE
inde on chqno+allt(str(amt,15,2)) 						tag chqamt
INDEX ON DTOS(pdate)+chqno+ALLTRIM(STR(amt,15,2))		TAG DATECHQAMT
inde  on pabank+dtos(pdate)+chqno+allt(str(amt,15,2)) tag pabankchq

Use data\Payee Excl
pack
Dele Tag all
index on ddate								tag ddate Desc
index on chqno								tag chqno
index on chqno+seq							tag chqseq
index on bCode+Dtos(pdate)+Code				tag bankcode
index on Dtos(pdate)+payee					tag pDate
index on bCode+Dtos(pDate)+Chqno			tag bcodechq
index on bCode+Dtos(pdate)+Code+chqno		tag bankcdchq
index on bCode+Dtos(pdate)+Code+chqno+ALLT(STR(AMOUNT,15,2))	tag bankcdchq1
index on bCode+Dtos(pdate)+Code+padl(allt(seq),6,' ')			tag bankcdseq
index on dtos(pdate)+chqno					tag pdatechq
INDEX on recid 								TAG recid 
Use data\Implog Excl
pack
Delete Tag all
index on Alltrim(fName)+Dtos(pdate)+Bcode+suffm	Tag implog



Wait Clear
