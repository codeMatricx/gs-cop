clos data
set excl on
use data\helpa orde usrkey in 0
use data\umast in 0

select distinct key_name,help_str from helpa into curs tmpcurs

sele helpa
if type("ucode")="U"
	alter table helpa add column ucode c(2)
endi

sele umast
go top
do whil !eof()

	m.ucode = ucode
	
	wait wind m.ucode nowa
	sele tmpcurs
	go top
	do whil !eof()
		scatt memv
		
		wait wind m.ucode+" : "+m.key_name nowa
		
		sele helpa
		if !seek(m.ucode+m.key_name)
			appe blan
			gather memv
		endi
		
		sele tmpcurs
		skip
	endd
	
	sele umast
	skip
endd
sele helpa
dele for empty(ucode)
pack
wait wind "User Wise Help Created" nowa
clos data
	
