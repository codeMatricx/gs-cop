*******************
procedure nullchk
*******************
para recnum
sele tmpcurs
go recnum
do whil !eof()
	scatt memv
	m.recno = allt(str(recno())) 
	thisform.olecontrol1.visible = .f.
	thisform.txt_accno.enabled = .f.
	thisform.refresh()
	if val(accno) = 0 or empty(accno)
		mimgpath = allt(m.imgpath)
		if !file("&mimgpath")
			wait wind "UNABLE TO RESTORE IMAGE FILE, PRESS ANY KEY TO CONTINUE..." NOWA
		else
			thisform.olecontrol1.visible = .t.
			thisform.olecontrol1.image = m.imgpath
			thisform.olecontrol1.display
			thisform.olecontrol1.rotateright
			thisform.olecontrol1.rotateright
			thisform.txt_accno.enabled = .t.
			thisform.txt_accno.visible = .t.	
			thisform.txt_accno.setfocus
			exit
		endi
	endi
	sele tmpcurs
	skip
endd
wait clear
thisform.refresh()


*******************
procedure notreq
*******************
DEFINE WINDOW minp;
		AT  30.000, 0.000  ;
		SIZE 5.350,40.615 ;
		TITLE "Input" ;
		FONT "ARIAL", 08 ;
		STYLE "B" ;
		FLOAT ;
		NOMINIMIZE ;
		COLOR RGB(,,,255,0,255)
sele tmpcurs
go top
do whil !eof()
	scatt memv
	m.recno = allt(str(recno())) 
	thisform.olecontrol1.visible = .f.
	thisform.txt_accno.enabled = .f.
	thisform.refresh()
	if val(accno) = 0 or empty(accno)
		mimgpath = allt(m.imgpath)
		if !file("&mimgpath")
			wait wind "UNABLE TO RESTORE IMAGE FILE, PRESS ANY KEY TO CONTINUE..." NOWA
		else
			thisform.olecontrol1.visible = .t.
			thisform.olecontrol1.image = m.imgpath
			thisform.olecontrol1.display
			thisform.olecontrol1.rotateright
			thisform.olecontrol1.rotateright
			acti window minp IN FORM
			show window minp
			do whil .t.
				macc = spac(12)
				myn  = 'Y'
				@ 0.500,2.000 SAY "Account #" get macc pict "999999999999" 	FONT "ARIAL", 08 STYLE "B"
				@ 2.500,2.000 SAY "Your Input OK [YN]?" get myn pict "@!" 	VALID myn $ 'YN' FONT "ARIAL", 08 STYLE "B"
				read
				if myn='N'
					loop
				else
					exit
				endi
			endd
			deac wind minp
		endi
	endi
	sele tmpcurs
	skip
endd
release wind minp
wait clear
m.recno = "Processed"
thisform.refresh()

****************************PROC SYSSET********************************
PROCEDURE SYSSET
ON KEY LABEL UPARROW
ON KEY LABEL DNARROW
ON KEY LABEL RIGHTARROW
ON KEY LABEL LEFTARROW
ON KEY LABEL ESC
ON KEY LABEL BACKSPACE
ON KEY LABEL F10
CLEAR
INSMODE(.F.)
NUMLOCK(.T.)
CAPSLOCK(.T.)
SET SAFE OFF
SET STAT OFF
SET TALK OFF 
SET EXAC OFF
SET NEAR OFF
SET CONF OFF
SET EXCL OFF
SET ECHO OFF
SET ESCA OFF
SET REPR TO -1
SET DEVI TO SCREE
SET DATE TO BRITISH
SET CURR TO 'Rs '
SET HOUR TO 24
SET CENT ON
SET CLOC ON
SET CURS ON
SET DELE ON
SET DEVE ON
SET OPTI ON
SET PALE ON
SET AUTO ON
_screen.caption  = "Overnite Express Limited - (Accounts Software)"

procedure showbusy
DEFINE WINDOW mbusy;
		AT  0.000, 0.000  ;
		SIZE 3.350,14.615 ;
		TITLE "Busy" ;
		FONT "ARIAL", 15 ;
		STYLE "B" ;
		FLOAT ;
		NOCLOSE ;
		NOMINIMIZE ;
		SYSTEM ;
		COLOR RGB(,,,255,0,255)
MOVE WINDOW mbusy CENTER
acti window mbusy
@ 1.300,2.923 SAY "Please Wait.." ;
	FONT "ARIAL", 10 ;
	STYLE "B"
return

procedure closebusy
if wexist('mbusy')
	deac wind mbusy
endi
return


* ------------- *
PROC seleuser
* ------------- *
CREA CURS user11 (ufld1 C(4), ufld3 C(8))

SELE user
SET ORDER TO TAG code
GO TOP
DO WHILE !EOF()
   SCAT MEMV
   
   fst   = .T.
   mUname= ""
   FOR i = 1 TO LEN(ALLT(m.ufld3))
       mUname = mUname+ CHR(ASC(SUBS(m.ufld3,i,1))-14)
   ENDFOR          
   
   SCAN WHILE ufld1 = m.ufld1 AND !EOF()
        IF fst
           SELE user11
           APPE BLAN
           REPL ufld1 WITH m.ufld1,;
                ufld3 WITH mUname
           fst = .F.
        ENDIF
        
        SELE user
   ENDSCAN
   
   SELE user
ENDDO

SELE user11
INDEX ON ufld3 Tag ufld3

no_of_prty = Recc()
DECLARE ap(no_of_prty)
For i = 1 TO no_of_prty
   ap(i) = .F.
EndFor
DECLARE sp(20)
	
sp(1) = 'user11'                                && dbf file name
sp(2) = 'ufld3'                                 && index name
sp(3) = 0                                       && index tag number
sp(4) = 'ufld3'                                 && search field name
sp(5) = ''                                      && first letter of searching field (Default searching)
sp(6) = ''                                      && filter
sp(7) = ''
sp(8) = ''
sp(9) = ''
sp(10)= ''
sp(11)= 'ap'
sp(12)=  3                                      && 
sp(13)= 'on key label f7 do unsel/<F7> to Unsel. All : <F8> to Select all'
sp(14)= 'on key label f8 do sel/<F8> to select on single character'
sp(15)= 'on key label f9 do sel_ch'

sp_ans = srchmult(@sp)
IF sp_ans >= 0
   CREATE CURS TmpUser (usercode C(4), username C(30))
   SELE user11
   SCAN
      IF ap(Recn())
         SELE TmpUser
         APPE BLAN
         REPL usercode WITH user11.ufld1 ,;
              username WITH user11.ufld3
      ENDIF
      SELE user11
   ENDSCAN
ENDIF
SELE user11
USE
SELE TmpUser
RETURN (sp_ans >= 0)


*---------------*
FUNC LOCKING
*---------------*
PARA pLock

DO WHILE .T.
   WAIT WIND "Please wait.... Record is already locked by different user." NOWA
   IF &pLock
      EXIT
   ENDIF
ENDDO
RETURN

*------------------*
func openawb
*------------------*
para pdate
do closeawb
fname=dpath+subs(cmont(pdate),1,3)+str(year(pdate),4)+'.dbf'
cfname=dpath+subs(cmont(pdate),1,3)+str(year(pdate),4)+'.cdx'

if !file("&fname") or !file("&cfname")
	retu .f.
else
	use &fname alias awbmast in 85
	retu .t.
endi

*---------------*
proc valerror
*---------------*
para errorfnd
if allt(str(erro()))='39'
	errorfnd=.t.
endi
return

*---------------*
proc direrror
*---------------*
para dirfnd
if allt(str(erro()))='202'
	dirfnd=.f.
endi
return

*******************
procedure nullchk
*******************
para recnum
sele tmpcurs
go recnum
do whil !eof()
	scatt memv
	m.recno = allt(str(recno())) 
	thisform.olecontrol1.visible = .f.
	thisform.txt_accno.enabled = .f.
	thisform.refresh()
	if val(accno) = 0 or empty(accno)
		mimgpath = allt(m.imgpath)
		if !file("&mimgpath")
			wait wind "UNABLE TO RESTORE IMAGE FILE, PRESS ANY KEY TO CONTINUE..." NOWA
		else
			thisform.olecontrol1.visible = .t.
			thisform.olecontrol1.image = m.imgpath
			thisform.olecontrol1.display
			thisform.olecontrol1.rotateright
			thisform.olecontrol1.rotateright
			thisform.txt_accno.enabled = .t.
			thisform.txt_accno.visible = .t.	
			thisform.txt_accno.setfocus
			exit
		endi
	endi
	sele tmpcurs
	skip
endd
wait clear
thisform.refresh()

***************************
* ---------------- *
Func DecrypUnm
* ---------------- *
para Ecrnm
retnm=''
for i=1 to len(allt(Ecrnm))
	retnm=retnm+chr(asc(subs(allt(Ecrnm),i,1))-100)
next
retu retnm

* ---------------- *
Func EcrypUnm
* ---------------- *
para Dcrnm
retnm=''
for i=1 to len(allt(Dcrnm))
	retnm=retnm+chr(asc(subs(allt(Dcrnm),i,1))+100)
next
retu retnm
***************************
* ---------------- *
Func DecrypPws
* ---------------- *
para EcrPws
retPws=''
for i=1 to len(allt(EcrPws))
	retPws=retPws+chr(asc(subs(allt(EcrPws),i,1))-90)
next
retu retPws

* ---------------- *
Func EcrypPws
* ---------------- *
para DcrPws
retPws=''
for i=1 to len(allt(DcrPws))
	retPws=retPws+chr(asc(subs(allt(DcrPws),i,1))+90)
next
retu retPws
**************************
* ---------------- *
Func DecrypGen
* ---------------- *
para Ecrdata,vl
ecrdata = allt(ecrdata)
retdata=''
for i=1 to len(Ecrdata)
	retdata=retdata+chr(asc(subs(Ecrdata,i,1))-vl)
next
retu retdata


* ---------------- *
Func EcrypGen
* ---------------- *
para Dcrdata,vl
dcrdata = allt(dcrdata)
retdata=''
for i=1 to len(Dcrdata)
    retdata=retdata+chr(asc(subs(dcrdata,i,1))+vl) 
next
retu retdata

PROCEDURE EXITPRG
DEACT WIND ALL
SET SYSMENU TO DEFAULT
SET CENTURY ON
SET CLOCK OFF
SET HOUR TO 24
SET CONFIRM ON
SET CURRENCY TO 'Rs.'
SET CURSOR ON
SET DATE TO BRITISH
SET DELETED OFF
SET DEVELOPMENT OFF
SET ESCAPE ON
SET EXCLUSIVE ON
SET HOURS TO 24
SET OPTIMIZE OFF
SET PALETTE OFF
SET READBORDER OFF
SET REPROCESS TO AUTOMATIC
SET SAFETY ON
SET STAT OFF
SET TALK off
DEACT WIND ALL
*clea all
clos all
_screen.caption  = "Microsoft Visual FoxPro"
=insmode(.t.)
=CAPSLOCK(.f.)
CANCEL

* ---------------- *
Function Chkrights
* ---------------- *
para pfilename,pbankcode

brmsg  = .f.
Retval = .t.
muname = ecrypgen(guname,100)
mpname = ecrypgen(pfilename,100)
muname = padr(muname,10,' ')
mpname = padr(mpname,30,' ')

if inlist(guname,'ADMIN')
	retval = .t.
else
	sele pmast
	set orde to pname
	if !Seek(mpname)
		wait wind "Programm Error" nowait
		retval = .f.
	else
		m.ptype = ptype

		Sele rights	
		Set order to ubank
		If !Seek(muname+mpname)
			Retval = .f.
		  Else
			if m.ptype = '0'
				If !Seek(muname+mpname+pbankcode)
					brmsg  = .t.
					Retval = .f.
				 else
					Retval = .t.
				Endif
			Endif	
		Endi
	endi
endi
Return Retval

* ---------------- *
Function GetBr
* ---------------- *
Parameter _pBank
lAlias = Alias()
Retval = ''
Sele Brnmst
Set Order to Pa_bank
If Seek( _pBank )
	Retval = BrName
Endif
Sele ( lAlias )
Return Retval


* ---------------- *
Function GetBr1
* ---------------- *
Parameter _pBank
lAlias = Alias()
Retval = ''
Sele Brnmst
Set Order to BrCode
If Seek( _pBank )
	Retval = BrName
Endif
Sele ( lAlias )
Return Retval

* ---------------- *
Function GetBr2
* ---------------- *
Parameter _pBank
lAlias = Alias()
Retval = ''
Sele Brnmst
Set Order to BrCode
If Seek( _pBank )
	Retval = Pa_Bank
Endif
Sele ( lAlias )
Return Retval

* ---------------- *
Function GetBank
* ---------------- *
Set Confirm On
Sele BrnMst
Set Order to BrName
Define Popup Xyz From 1,1 Prompt Fields Left(BrName,25)+' '+Pa_Bank Shadow Margin Font 'FixedSys', 10 Title '..... Select Bank ....'
On Selection popup Xyz Deactivate popup Xyz
Activate popup Xyz
Retval = Pa_bank
Set Confirm Off
Return Retval

*------------------*
Function GetChk
*------------------*
Parameter _pPara
Retval = .t.
kAlias = Alias()
Sele Sorter
Set Order to bcodechq
If Seek(_pPara)
    Retval = .f. 
Endif
Sele ( kAlias )
Return Retval

*------------------*
Function GetChk2
*------------------*
Parameter _pPara
Retval = .t.
kAlias = Alias()
Sele Drawmick
Set Order to pbankchq
If Seek(_pPara)
    Retval = .f. 
Endif
Sele ( kAlias )
Return Retval

* ---------------- *
Function GetBank1
* ---------------- *
Set Confirm On
Sele BrnMst
Set Order to BrName
Define Popup Xyz From 1,1 Prompt Fields Left(BrName,25)+' '+Pa_Bank+' '+BrCode ;
Shadow Margin Font 'FixedSys', 10 Title '..... Select Bank ....'
On Selection popup Xyz Deactivate popup Xyz
Activate popup Xyz
Retval = BrCode+Right(Pa_bank,3)
Set Confirm Off
Return Retval

* ---------------- *
Function Gettc1
* ---------------- *
Parameter _pStr
Retval = ''
Do Case
	Case _pStr = 'M1'
		Retval = '11'
	Case _pStr = 'M2'
		Retval = '31'
	Case _pStr = 'M4'
		Retval = '18'
	Case _pStr = 'M6'
		Retval = '13'
	Case _pStr = 'M8'
		Retval = '10'
	Case Left(_pStr,1) = 'R'
		Retval = '29'
Endcase	
Return Retval


* ---------------- *
Function Gettc
* ---------------- *
Set Confirm On
lAlias = Alias()
Sele TcMast
Define Popup Xyz From 1,1 Prompt Fields TcCode in Screen Shadow Margin Font 'FixedSys', 10 Title '.. Tc Code ..'
On Selection popup Xyz Deactivate popup Xyz
Activate popup Xyz
Set Confirm Off
Retval = TcCode
Sele ( lAlias )
Return Retval

* ---------------- *
Function GetRea
* ---------------- *
Set Confirm On
lAlias = Alias()
Sele Retu1
Set Order to Reason
Define Popup Xyz From 1,1 Prompt Fields Reason in Screen Shadow Margin Font 'FixedSys', 10 Title '... Select Reason ...'
On Selection popup Xyz Deactivate popup Xyz
Activate popup Xyz
Set Confirm Off
mSa = Sav_Chrgs
mCa = Ca_Chrgs
Sele ( lAlias )
Return

* ---------------- *
Function GetCode
* ---------------- *
_pOrder = Order()
mRec = Recno()
Set Order to uCode desc
Go Top
Retval = PADL(Allt(Str(Val(uCode)+1)),'0',3)
Set Order to ( _pOrder )
Go mRec
Return Retval


* ------------- *
PROC Selusr
* ------------- *
SELE umast
SET ORDER TO TAG fld1

create curs cursusr(uname C(10), fld1 c(10))
inde on uname tag uname

sele umast
go top
do whil !eof()
	scatt memv
	
	sele cursusr
	appe blan
    repl uname with DecrypGen(umast.fld1,100), fld1  with umast.fld1
    
    sele umast
    skip
endd

no_of_prty = Recc()
DECLARE ap(no_of_prty)
For i = 1 TO no_of_prty
   ap(i) = .F.
EndFor
DECLARE sp(20)
	
sp(1) = 'cursusr'                               && dbf file name
sp(2) =  'uname'                               && index name
sp(3) = 0                                       && index tag number
sp(4) = 'uname'                               && search field name
sp(5) = ''                                      && first letter of searching field (Default searching)
sp(6) = ''										&& filter  'LEFT(destcode,2) = LEFT(gmStncode,2)'
sp(7) = ''
sp(8) = ''
sp(9) = ''
sp(10)= ''
sp(11)= 'ap'
sp(12)=  3                                      && 
sp(13)= 'on key label f7 do unsel/<F7> to Unsel. All : <F8> to Select all'
sp(14)= 'on key label f8 do sel/<F8> to select on single character'
sp(15)= 'on key label f9 do sel_ch'

sp_ans = srchmult(@sp)
IF Sp_ans >= 0
   CREATE CURS TmpOffs (uname C(10), fld1 c(10))
   SELE cursusr
   SCAN
      IF ap(Recn())
         SELE TmpOffs
         APPE BLAN
         REPL uname WITH cursusr.uname, fld1  with cursusr.fld1
      ENDIF
      SELE cursusr
   ENDSCAN
ENDIF
RETURN (sp_ans >= 0)

* ------------- *
PROC Seltc
* ------------- *
SELE tcmast
SET ORDER TO TAG tccode

create curs cursusr(tccode C(2), fld1 c(10))
inde on tccode tag tccode

sele tcmast
go top
do whil !eof()
	scatt memv
	
	sele cursusr
	appe blan
    repl tccode with tcmast.tccode
    
    sele tcmast
    skip
endd

no_of_prty = Recc()
DECLARE ap(no_of_prty)
For i = 1 TO no_of_prty
   ap(i) = .F.
EndFor
DECLARE sp(20)
	
sp(1) = 'cursusr'                               && dbf file name
sp(2) =  'tccode'                               && index name
sp(3) = 0                                       && index tag number
sp(4) = 'tccode'                               && search field name
sp(5) = ''                                      && first letter of searching field (Default searching)
sp(6) = ''										&& filter  'LEFT(destcode,2) = LEFT(gmStncode,2)'
sp(7) = ''
sp(8) = ''
sp(9) = ''
sp(10)= ''
sp(11)= 'ap'
sp(12)=  3                                      && 
sp(13)= 'on key label f7 do unsel/<F7> to Unsel. All : <F8> to Select all'
sp(14)= 'on key label f8 do sel/<F8> to select on single character'
sp(15)= 'on key label f9 do sel_ch'

sp_ans = srchmult(@sp)
IF Sp_ans >= 0
   CREATE CURS TmpOffs (tccode C(2), fld1 c(10))
   
   SELE cursusr
   SCAN
      IF ap(Recn())
         SELE TmpOffs
         APPE BLAN
         REPL tccode WITH cursusr.tccode, fld1  with cursusr.fld1
      ENDIF
      SELE cursusr
   ENDSCAN
ENDIF
RETURN (sp_ans >= 0)

*--*-*-*-*-*-*-*-*-*-*-*-*-
FUNCTION removespcharacters
*--*-*-*-*-*-*-*-*-*-*-*-*-
para pstring

rval = ''
if empty(pstring)
	retu rval
else
	stringlen = len(allt(pstring))
	for a = 1 to stringlen
	
		rchar  = substr(pstring,a,1)
		chkval = asc(rchar)
		IF BETWEEN(chkval,48,57) OR BETWEEN(chkval,65,90) OR BETWEEN(chkval,97,122) OR INLIST(chkval,47,92,45)
			rval = rval + rchar
		ELSE
			rval = rval + SPACE(1)
		ENDIF
	next
endi
retu allt(rval)

*---------------------------------------------
procedure create_excel_file_dump
*---------------------------------------------
*!*	This Procedure will Generate Excel Dump with Customised Header Name
*!*	Takes Parameter of Excel FIle Name  & Max. of 25 Column/Header Name

parameter p_xlsfile, hd1, hd2, hd3, hd4, hd5, hd6, hd7, hd8, hd9, hd10, hd11, hd12, ;
		  hd13, hd14, hd15, hd16, hd17, hd18, hd19, hd20,hd21, hd22, hd23,hd24,hd25

c_total_headers_passed =  PARAMETERS() - 1
c_total_headers_passed = IIF( c_total_headers_passed >25, 25, c_total_headers_passed )

mexcel = createobject("excel.application")
mexcel.workbooks.open(p_xlsfile)
mexcel.rows("1:1").select
mexcel.selection.font.bold = .t.
mexcel.rows("2:2").select
mexcel.activewindow.freezepanes = .t.
COLUMN_NUMBER = "A"

for ctr = 1 to c_total_headers_passed	&& Printing Headers

	p_fld = 'HD'+alltrim(str(ctr))
	p_cell = COLUMN_NUMBER + "1"
	if !empty( &p_fld )
		mexcel.range(p_cell).select
		mexcel.activecell.formular1c1 = &p_fld
		COLUMN_NUMBER = GET_NEXT_COLUMN( COLUMN_NUMBER )
	endif
	
endfor
mexcel.cells.select
with mexcel.selection.font
	.name = "Arial"  && "Arial Narrow"
	.size = 8
endwith
mexcel.selection.columns.autofit
*!*	mexcel.Selection.HorizontalAlignment = -4108 && xlCenter    
mexcel.range("A1").select
mexcel.visible = .t.
return
*---------------------------------------------
* EOF :: create_excel_file_dump  
*---------------------------------------------
