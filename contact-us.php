<?php
if(($_POST) || isset($_FILES['my_file']))
{

    $from_email         = 'gsfcconline@gmail.com'; //from mail, it is mandatory with some hosts
    $recipient_email    = 'gsfcconline@gmail.com'; //recipient email (most cases it is your personal email)
    $subject           = 'Application-Form';

    //Capture POST data from HTML form and Sanitize them, 

    $sender_name    = filter_var($_POST["name"], FILTER_SANITIZE_STRING); //sender name
    $reply_to_email = filter_var($_POST["email"], FILTER_SANITIZE_STRING); //sender email used in "reply-to" header
 
    $message        = filter_var($_POST["message"], FILTER_SANITIZE_STRING); //message
    $details= "Name: " . $sender_name .  "\r\n" . "Email: " . $reply_to_email. "\r\n" . "Message: ".  $message;
  
    /* // validate empty fields 
    if(strlen($sender_name)<1){
        die('Name is too short or empty!');
    } 
    */
    
    //Get uploaded file data
    $file_tmp_name    = $_FILES['my_file']['tmp_name'];
    $file_name        = $_FILES['my_file']['name'];
    $file_size        = $_FILES['my_file']['size'];
    $file_type        = $_FILES['my_file']['type'];
    $file_error       = $_FILES['my_file']['error'];

    

    
    //read from the uploaded file & base64_encode content for the mail
     $handle = fopen($file_tmp_name, "r");
    $content = fread($handle, $file_size);
    fclose($handle);
    $encoded_content = chunk_split(base64_encode($content));

        $boundary = md5("sanwebe");
        //header
        $headers = "MIME-Version: 1.0\r\n"; 
        $headers .= "From:".$from_email."\r\n"; 
        $headers .= "Reply-To: ".$reply_to_email."" . "\r\n";
        $headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n"; 
        
        //plain text 
        $body = "--$boundary\r\n";
        $body .= "Content-Type: text/plain; charset=ISO-8859-1\r\n";
        $body .= "Content-Transfer-Encoding: base64\r\n\r\n"; 
        $body .= chunk_split(base64_encode($details)); 
    
    
        //attachment
        $body .= "--$boundary\r\n";
        $body .="Content-Type: $file_type; name=".$file_name."\r\n";
        $body .="Content-Disposition: attachment; filename=".$file_name."\r\n";
        $body .="Content-Transfer-Encoding: base64\r\n";
        $body .="X-Attachment-Id: ".rand(1000,99999)."\r\n\r\n"; 
        $body .= $encoded_content; 
    
    $sentMail = @mail($recipient_email, $subject, $body, $headers);
    if($sentMail) //output success or failure messages
    {       
      echo "<SCRIPT type='text/javascript'>
               alert('email has been sent');
              window.location.replace('http://staging.isiwal.com/gsccop.co.in/');
         </SCRIPT>";
    }else{
        echo "<SCRIPT type='text/javascript'>
               alert('email has been sent');
              window.location.replace('http://staging.isiwal.com/gsccop.co.in/');
         </SCRIPT>";
    }

}
?>

<?php 
include "admin/database.php";
 $sql1 ="SELECT name, mobile, image, heading from profile";
 $sql2 ="SELECT name, wishes from greetings";
 $result1 = $conn->query($sql1);
 $result2 = $conn->query($sql2);
 if($result1->num_rows > 0)
{
    $profile = $result1->fetch_assoc();
}
 if($result2->num_rows > 0)
{
   $profile1 = $result2->fetch_assoc();
 }
 ?>

<!DOCTYPE html>
<!--
Template Name: Crafting
Author: <a href="http://www.os-templates.com/">OS Templates</a>
Author URI: http://www.os-templates.com/
Licence: Free to use under our free template licence terms
Licence URI: http://www.os-templates.com/template-terms
-->
<html>
<head>
<title></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
</head>
<body id="">
<!-- ################################################################################################ --> 
<!-- ################################################################################################ --> 
<!-- ################################################################################################ -->
<div class=" row1 bgded" style="background-color: #00529d;">
  <div class="overlay">
    <header id="" class="clear"> 
      <!-- ################################################################################################ -->
     <div class="group btmspace-50 demo">
        <div class="one_quarter first" style=""><div id="logo" style="margin-top: 20px;">
        <h1 style="font-size: 64px; margin-left: 15px;">GSCOP</h1>
      </div></div>
        <div class="two_quarter"><div class="greetings" style="font-size: 25px; margin-top: 20px;">
                                                          <div class="blog_sid_content" style="margin-left:90px;">
                                                               <b> <?php echo isset($profile1['wishes'])?$profile1['wishes']:"wishes"; ?></b>
                                                               <p> <?php echo isset($profile1['name'])?$profile1['name']:"name"; ?></p>
                                                              <a href="greetingspage.php"><button type="button" style="cursor:pointer; background:white; margin-left: 90px;">Click Me!</button></a>
                                 
                          
                                                             </div>   
                                                            </div> </div>
        <div class="one_quarter" style="font-size: 19px; margin-top: 20px;"> <img src="http://staging.isiwal.com/licadmincrm/admin/assets/img/profile/<?php echo isset($profile['image'])?$profile['image']:"image"; ?>" class="header-image1" alt="" style="width:140px;height:110px; float: right; margin-right: 20px;">
                                                            <div class="header-image2">
                                                               <b> <?php echo isset($profile['heading'])?$profile['heading']:"heading"; ?></b>
                                                               <p> <?php echo isset($profile['name'])?$profile['name']:"name"; ?> <br> Mobile no. <?php echo isset($profile['mobile'])?$profile['mobile']:"mobile"; ?></p>
                                                            </div></div>
      </div>
    </header>
  </div>
</div>
<div class=" row1 bgded" style="color: white;">
  <div class="overlay">
    <header id="" class="clear" style="margin-top: -10px;"> 
      <!-- ################################################################################################ -->
     
     <nav id="mainav" class="clear" style="margin-top: -40px;">
        <ul class="clear">
          <li class="active"><a href="index.php">Home</a></li>
          <li><a class="drop" href="#">Services
    </a>
            <ul>
              <li><a href="#">HANDLING OF INWARD CLEARING</a></li>
              <li><a href="#">CAPTURE IMAGE OF CTS CHQUES</a></li>
              <li><a href="#">HANDLING OF INT./DIVIDEND WARRANTS.</a></li>
              <li><a href="#">PDC MANAGEMENT.(POST DATED CHEQUES)</a></li>
              <li><a href="#">HANDLING OF OUTWARD CLEARING.(CTS)</a></li>
            </ul>
          </li>
          <li><a href="about.php">About us</a></li>
          <li><a class="drop" href="#">Utilities
    </a>
            <ul>
              <li><a href="attachments/gscop.pdf">About GS Cop</a></li>
              <li><a href="product.php">Products</a></li>
             
            </ul>
          </li>
          <!-- <li><a href="#">Achievements</a></li> -->
          <li><a href="contact-us.php">Contact Us</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ --> 
    </header>
  </div>
</div>
<!-- <div class="wrapper row2">
  <div id="breadcrumb" class="clear">
    
    <ul>
      <li><a href="#">Home</a></li>
      <li><a href="#">Lorem</a></li>
      <li><a href="#">Ipsum</a></li>
      <li><a href="#">Dolor</a></li>
    </ul>
   
  </div>
</div> -->
<div class="wrapper row3" style="background-color: #f1f1f1; width: 100%;">
  <main class="" style="margin: 0px;"> 
    <!-- main body --> 
    <!-- ################################################################################################ -->
   
   <div class="group btmspace-50 demo">
     <div class="three_quarter  first"><h2 class="title-style-2" style="text-align: center; margin-top: 60px;">CONTACT FORM <span class="title-under"></span></h2>
                                                      <form action="#" class="" method="POST" enctype="multipart/form-data" style="text-align: -webkit-center">
                                                         <div class="row">
                                                            <div class="form-group">
                                                               <input type="text" name="name" class="form1" placeholder="Name*" required="" style="height: 34px; width: 300px; margin-top: 10px;">
                                                            </div>
                                                            <div class="form-group">
                                                               <input type="email" name="email" class="form1" placeholder="E-mail*" required="" style="height: 34px; width: 300px; margin-top: 10px;">
                                                            </div>
                                                         </div>
                                                         <div class="form-group">
                                                            <textarea name="message" rows="5" cols="35" class="form1" placeholder="Message*" required="" style="margin-top: 10px;"></textarea>
                                                         </div>
                                                         <div class="form-group alerts">
                                                            <div class="alert alert-success" role="alert">
                                                            </div>
                                                            <div class="alert alert-danger" role="alert">
                                                            </div>
                                                         </div>
                                                          <div class="form-group">
                                                              <label><input type="file" name="my_file" /></label><br>
                                                           
                                                         </div>

                                                         <div class="form-group">
                                                            <label><input type="submit" name="button" value="Submit" /></label>
                                                         </div>
                                                         <div class="clearfix"></div>
                                                      </form></div>
        <div class="one_quarter"></div>
         <h2 class="title-style-2" style="margin-top:30px; text-align: -webkit-center;"> MY CONTACT <span class="title-under"></span></h2>
                                                      <p>
                                                         <b style="margin-left:120px;">Sanjay Sharma</b>
                                                      </p>
                                                      <div class="">
                                                         <ul class="">
                                                            <li class=""> <span class="contact-icon"> <i class="fa fa-map-marker"></i></span><b>Address 1</b><br>E-1 JHANDEWALAN EXT , BALUJA HOUSE ,DELHI 110055<br><b>Address 2</b><br> G S COMPUTER CONSULTANCY 496 ,  UDYOG KENDRA II , ECOTECH III,<br>nbsp GREATER NOIDA<br><b>BRANCH LOCATION</b><br>BRANCH 113 , JEEVAN TARA BUILDING ,<br>1ST FLOOR GATE NO 4-5, SANSAD MARG ,<br> NEW DELHI 110001
                                                            </li>
                                                            <li class="contact-item"> <span class="contact-icon"> <i class="fa fa-phone"></i></span>9891478002,9212221506,9891478007,011-41540139/23593313</li>
                                                            <li class="contact-item"> <span class="contact-icon"> <i class="fa fa-envelope"></i></span> gsfcconline@gmail.com,info@gscc.co.in</li>
                                                         </ul>
       
      </div>
</div>
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>
<div class="wrapper row3" style="">
   
<header>
    <div id="map" style="width:100%;height:350px;background:#ccc"></div>
                                          <script>
                                             function initMap() {

  var broadway = {
    info: '<strong>Licindiancr</strong><br>\
          E-1 JHANDEWALAN EXT,BALUJA HOUSE,DELHI 110055<br> DELHI 110055<br>\
          ',
    lat: 28.645116,
    long: 77.204221
  };

  var belmont = {
    info: '<strong>Licindiancr</strong><br>\
          G S COMPUTER CONSULTANCY 496 ,UDYOG KENDRA II , ECOTECH III,<br> GREATER NOIDA,India<br>\
          ',
    lat: 28.533243,
    long: 77.469093
  };

  

  var locations = [
      [broadway.info, broadway.lat, broadway.long, 0],
      [belmont.info, belmont.lat, belmont.long, 1],
      
    ];

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 10,
    center: new google.maps.LatLng(28.620764, 77.363929),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var infowindow = new google.maps.InfoWindow({});

  var marker, i;

  for (i = 0; i < locations.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
      map: map
    });

    google.maps.event.addListener(marker, 'click', (function (marker, i) {
      return function () {
        infowindow.setContent(locations[i][0]);
        infowindow.open(map, marker);
      }
    })(marker, i));
  }
}

                                          </script>
                                          <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8qngVObWhwvfsIOivE7eZjXwUJ9Qq_Xk&callback=initMap"></script>
                                          </header>
  
</div>

<div class="wrapper row5">
  <div id="copyright" class="clear"> 
    <!-- ################################################################################################ -->
    <p style="text-align: center;">&copy; 1989-2017. GS COP Ltd . All Rights Reserved.</p>
    <!-- ################################################################################################ --> 
  </div>
</div>
<!-- JAVASCRIPTS --> 
<script src="layout/scripts/jquery.min.js"></script> 
<script src="layout/scripts/jquery.mobilemenu.js"></script>
</body>
</html>