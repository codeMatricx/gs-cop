<?php 
include "admin/database.php";
 $sql1 ="SELECT name, mobile, image, heading from profile";
 $sql2 ="SELECT name, wishes from greetings";
 $result1 = $conn->query($sql1);
 $result2 = $conn->query($sql2);
 if($result1->num_rows > 0)
{
    $profile = $result1->fetch_assoc();
}
 if($result2->num_rows > 0)
{
   $profile1 = $result2->fetch_assoc();
 }
 ?>

<!DOCTYPE html>

<html>
<head>
<title></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">

<script src="layout/scripts/jquery.min.js"></script> 
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<style type="text/css">
  
   .button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 0px 22px 0px 22px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
  height:30px;
}
.buyformdiv,.demoformdiv
{
  border-radius: 10px;
  display:none; 
  position:fixed; 
  top:50%; 
  left:50%; 
  transform: translate(-50%,-50%); 
  z-index:30001;
  height: 240px;
  width: 400px;
  padding:20px 50px;
  background-color: #c7b8b8;
}
.buyformdiv form,.demoformdiv form
{
  box-sizing: border-box;
  text-align: center;
}
.buyformdiv form input,.demoformdiv form input
{
  padding:5px;
  margin: 12px auto;
  width: calc(100% - 10px);
}
.buyformdiv form input[type='submit'],.demoformdiv form input[type='submit']
{
  width : 100%;
}
<!--.my-price{
border: none;
    color: white;
    padding: 0px 22px 0px 22px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}-->
.button3 {border-radius: 8px;}
</style>
<script type="text/javascript">
  
  $(document).ready(function() {
        $(".buy").click(function(event) {
          $("#buyform").show();
          $("#productname").val($(this).siblings('.productname').val());
          $("#productprice").val($(this).siblings('.productprice').val());
          $("#fulllink").val($(this).siblings('.productfulllink').val());
          event.stopPropagation();
        });
        $('body').click(function() {
           $('#buyform').hide();
        });
        $('.buyformdiv').click(function(event){
           event.stopPropagation();
        });
        $(".demo").click(function(event) {
          $("#demolink").val($(this).siblings('.productdemolink').val());
          $("#demoform").show();
          event.stopPropagation();
        });
        $('body').click(function() {
           $('#demoform').hide();
        });
        $('.demoformdiv').click(function(event){
           event.stopPropagation();
        });
      });
</script>
</head>


<body id="abc">
<!-- ################################################################################################ --> 
<!-- ################################################################################################ --> 
<!-- ################################################################################################ -->
<div class=" row1 bgded" style="background-color: #00529d;">
  <div class="overlay">
    <header id="" class="clear"> 
      <!-- ################################################################################################ -->
     <div class="group btmspace-50 demo">
        <div class="one_quarter first" style=""><div id="logo" style="margin-top: 20px; margin-left: 15px;">
        <h1 style="font-size: 64px; margin-left: 15px;">GSCOP</h1>
      </div></div>
        <div class="two_quarter"><div class="greetings" style="font-size: 25px; margin-top: 20px;">
                                                          <div class="blog_sid_content" style="margin-left:90px;">
                                                               <b> <?php echo isset($profile1['wishes'])?$profile1['wishes']:"wishes"; ?></b>
                                                               <p> <?php echo isset($profile1['name'])?$profile1['name']:"name"; ?></p>
                                                              <a href="greetingspage.php" style="margin-left: 90px;"><button type="button" style="cursor:pointer; background:white;">Click Me!</button></a>
                                 
                          
                                                             </div>   
                                                            </div> </div>
        <div class="one_quarter" style="font-size: 19px; margin-top: 20px;"> <img src="admin/assets/img/profile/<?php echo isset($profile['image'])?$profile['image']:"image"; ?>" class="header-image1" alt="" style="width:140px;height:110px; float: right; margin-right: 20px;">
                                                            <div class="header-image2">
                                                               <b> <?php echo isset($profile['heading'])?$profile['heading']:"heading"; ?></b>
                                                               <p> <?php echo isset($profile['name'])?$profile['name']:"name"; ?> <br> Mobile no. <?php echo isset($profile['mobile'])?$profile['mobile']:"mobile"; ?></p>
                                                            </div></div>
      </div>
    </header>
  </div>
</div>
<div class=" row1 bgded" style="color: white;">
  <div class="overlay">
    <header id="" class="clear" style="margin-top: -10px;"> 
      <!-- ################################################################################################ -->
     
       <nav id="mainav" class="clear" style="margin-top: -40px;">
        <ul class="clear">
          <li class="active"><a href="index.php">Home</a></li>
          <li><a class="drop" href="#">Services
    </a>
            <ul>
              <li><a href="#">HANDLING OF INWARD CLEARING</a></li>
              <li><a href="#">CAPTURE IMAGE OF CTS CHQUES</a></li>
              <li><a href="#">HANDLING OF INT./DIVIDEND WARRANTS.</a></li>
              <li><a href="#">PDC MANAGEMENT.(POST DATED CHEQUES)</a></li>
              <li><a href="#">HANDLING OF OUTWARD CLEARING.(CTS)</a></li>
            </ul>
          </li>
          <li><a href="about.php">About us</a></li>
          <li><a class="drop" href="#">Utilities
    </a>
            <ul>
              <li><a href="attachments/gscop.pdf">About GS Cop</a></li>
              <li><a href="product.php">Products</a></li>
             
            </ul>
          </li>
         <!--  <li><a href="#">Achievements</a></li> -->
          <li><a href="contact-us.php">Contact Us</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ --> 
    </header>
  </div>
</div>
<!-- <div class="wrapper row2">
  <div id="breadcrumb" class="clear">
    
    <ul>
      <li><a href="#">Home</a></li>
      <li><a href="#">Lorem</a></li>
      <li><a href="#">Ipsum</a></li>
      <li><a href="#">Dolor</a></li>
    </ul>
   
  </div>
</div> -->
<div class="wrapper row3" style="background-color: #f1f1f1; width: 100%;">
  <main class="" style="margin: 0px;"> 
    <!-- main body --> 
    <!-- ################################################################################################ -->
    <div class="group btmspace-50 demo">
     <?php 
                                    $sql = "SELECT * from products";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows>0)
                                    {
                                        
                                        
                                        while($products = $result->fetch_assoc())
                                        {
                                            
                                    ?>
                                    <div>
                                        <div class="one_quarter first" style="">
                                        <img src="admin/assets/img/products/<?php echo $products['image']; ?>" style="margin-left: 99px;margin-top: 73px; width:200px; height:200px;"></div>
<div class="two_quarter">
                                        <h3 style="margin-top: 60px;"><?php echo $products['name']; ?></h3>

                                        <p class="details" style="min-height:205px; max-height:205px; overflow-y:scroll;
                                        overflow-x:hidden;"> <?php echo $products['details']; ?></p>
                                       <div class="my-btn" style="text-align:right;">
                                        <input type="hidden" name="product_name" class="productdemolink" value="<?php echo $products['demo_product']; ?>">
                                        <input type="hidden" name="product_name" class="productfulllink" value="<?php echo $products['full_product']; ?>">

                                        <button class="button demo" style="text-align:right;">Demo</button>
                                        <button class="button buy" style="text-align:right;">Buy</button>

                                        <input type="hidden" name="product_name" class="productname" value="<?php echo $products['name']; ?>">
                                        <input type="hidden" name="product_price" class="productprice" value="<?php echo $products['price']; ?>">
                                      
                                        </div>


                                        <!-- <h4 style="margin-top:-47px; margin-left: 343px;">Rs:-</h4> -->
                                        <p style="margin-top: -35px;"><h4 style="display: inline;">Rs:-</h4> <?php echo $products['price']; ?></p>
                                       
                                        
                                        <hr style="";">
</div>
                                    <?php
                                        
                                         } } 
                                    ?>

          <div class="one_quarter">
            
             <!-- <form id="" class="" action="php/querymail.php" method="post" style="margin-left: 100px;">
                                                                           <div class="form-item"><label class="">Name <span class="form-required">*</span></label>
                                                                              <input type="text" id="" name="name" class="" required>
                                                                           </div>
                                                                           <div class="form-item"><label class="form-item-label">Mobile <span class="form-required">*</span></label>
                                                                              <input type="text"  name="field1" class="" required>
                                                                           </div>
                                                                           <div class="form-item"><label class="form-item-label">Your Address</label>
                                                                              <input type="text" name="field2" class="" required>
                                                                           </div>
                                                                           <div class="form-item"><label class="form-item-label">E-mail <span class="form-required">*</span></label>
                                                                              <input type="text"  name="mail" class="form-item-textfield required email" required>
                                                                           </div>
                                                                           <div class="form-item"><label class="form-item-label">Your Query <span class="form-required">*</span></label><textarea name="message" class="form-item-textarea expand100-200 required"></textarea>
                                                                           </div>
                                                                           <div class="form-note">Fields marked with <span class="form-required">*</span> are required.</div>
                                                                           <input type="submit" value="Send e-mail">
                                                                        </form> -->
                                                                        
          </div>
          
      </div>
   
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>

<div class="wrapper row5">
  <div id="copyright" class="clear"> 
    <!-- ################################################################################################ -->
   <p style="text-align: center;">&copy; 1989-2017. GS COP Ltd . All Rights Reserved.</p>
    <!-- ################################################################################################ --> 
  </div>
</div>
<!-- JAVASCRIPTS --> 



<div class="demoformdiv" id="demoform">
        <form action="mailphp/mail.php" method="post">
        <h2>Fill Details Here</h2>
          <input type="text" name="name" placeholder="Name">
          <input type="email" name="email" placeholder="Email">
          <input type="text" name="mobile" placeholder="Mobile">
          <input type="hidden" id='demolink' name="downloadlink" >
          <input type="submit" class="button" value="Demo Download" style="cursor:pointer;">
        </form>
      </div>
      <div class="buyformdiv" id="buyform" style="top:53%; height:270px;">
        <form action="pay.php" method="post">
        <h2>Fill Detail Here</h2>
          <input type="text" name="name" placeholder="Name">
          <input type="email" name="email" placeholder="Email">
          <input type="text" name="mobile" placeholder="Mobile">
      <input type="text" name="address" placeholder="Address">
          <input type="hidden" value="" name="product_name" id="productname" >
          <input type="hidden" id='fulllink' name="downloadlink" >
          <input type="hidden" value="" name="product_price" id="productprice">
          <input type="submit" class="button" value="Buy Now" style="cursor:pointer;">
        </form>
      </div>


</body>
</html>