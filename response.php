<?php
// header('Access-Control-Allow-Origin: *');
include "database.php";
$paymentid = $_GET['payment_request_id'];
$requesturl = "https://www.instamojo.com/api/1.1/payment-requests/$paymentid/";

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $requesturl );
curl_setopt($ch, CURLOPT_HEADER, FALSE);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
curl_setopt($ch, CURLOPT_HTTPHEADER,
            array("X-Api-Key:0c06cb231ed84fac061fbaa17531aef9",
                  "X-Auth-Token:5cbcdac4e46d237c8abb01904682ad1a"));
$response = curl_exec($ch);
curl_close($ch); 

$response = json_decode($response);

$name = $response->payment_request->buyer_name;
$email = $response->payment_request->email;
// $date = date("Y-m-d");
$price = $response->payment_request->amount;
$payment_id = $_GET['payment_id'];
$payment_request_id = $_GET['payment_request_id'];
// if($response->payment->status == 'credit')
if ($response->payment_request->payments[0]->status == 'credit') 
{ 
  $title = "Success";
  $paymentMessage = "Your Payment is Successfully submitted";
  $responseMessage = "Thank you for purchasing our software";
  
  }
else
{
  $title = "Sorry";
  $paymentMessage = "Your Payment was Unsuccessful";
  $responseMessage = "Please Try Again Later.";
}

 ?>
<html>
  <head>
    <title><?php echo $title; ?></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="SHORTCUT ICON" href="image/favicon.ico" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
  <body>
    <div class="container">
      <div class="row text-center">
        <div class="col-sm-6 col-sm-offset-3">
          <br><br>
          <h2 style="color:#0fad00"><?php echo $paymentMessage; ?></h2>
          <p style="font-size:20px;color:#5C5C5C;"><?php echo $responseMessage; ?></p>
          <br><br>
          <a href="http://staging.isiwal.com/gscop/products.php" class="btn btn-success">Click here for Home page</a>
        </div>
      </div>
    </div>
  </body>
</html>



