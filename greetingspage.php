﻿<?php 
include "admin/database.php";
 $sql1 ="SELECT name, mobile,image, heading from profile";
 $sql2 ="SELECT name, wishes,images from greetings";
 $result1 = $conn->query($sql1);
 $result2 = $conn->query($sql2);
 if($result1->num_rows > 0)
 {
    $profile = $result1->fetch_assoc();
 }
 if($result2->num_rows > 0)
 {
   $profile1 = $result2->fetch_assoc();
 }
 ?>

<!DOCTYPE html>

<html>
<head>
<title></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">


<script src="layout/scripts/jquery.min.js"></script> 
<script src="layout/scripts/jquery.mobilemenu.js"></script>


</head>


<body id="abc">
<!-- ################################################################################################ --> 
<!-- ################################################################################################ --> 
<!-- ################################################################################################ -->
<div class=" row1 bgded" style="background-color: #00529d;">
  <div class="overlay">
    <header id="" class="clear"> 
      <!-- ################################################################################################ -->
     <div class="group btmspace-50 demo">
        <div class="one_quarter first" style=""><div id="logo" style="margin-top: 20px; margin-left: 15px;">
         <h1 style="font-size: 64px; margin-left: 15px;">GSCOP</h1>
      </div></div>
        <div class="two_quarter"><div class="greetings" style="font-size: 25px; margin-top: 20px;">
                                                          <div class="blog_sid_content" style="margin-left:90px;">
                                                               <b> <?php echo isset($profile1['wishes'])?$profile1['wishes']:"wishes"; ?></b>
                                                               <p> <?php echo isset($profile1['name'])?$profile1['name']:"name"; ?></p>
                                                             <!-- <a href="greetingspage.php" style="margin-left: 90px;"><button type="button" style="cursor:pointer; background:white;">Click Me!</button></a>-->
                                 
                          
                                                             </div>   
                                                            </div> </div>
        <div class="one_quarter" style="font-size: 19px; margin-top: 20px;"> <img src="admin/assets/img/profile/<?php echo isset($profile['image'])?$profile['image']:"image"; ?>" class="header-image1" alt="" style="width:140px;height:110px; float: right; margin-right: 20px;">
                                                            <div class="header-image2">
                                                               <b> <?php echo isset($profile['heading'])?$profile['heading']:"heading"; ?></b>
                                                               <p> <?php echo isset($profile['name'])?$profile['name']:"name"; ?> <br> Mobile no. <?php echo isset($profile['mobile'])?$profile['mobile']:"mobile"; ?></p>
                                                            </div></div>
      </div>
    </header>
  </div>
</div>
<div class=" row1 bgded" style="color: white;">
  <div class="overlay">
    <header id="" class="clear" style="margin-top: -10px;"> 
      <!-- ################################################################################################ -->
     
       <nav id="mainav" class="clear" style="margin-top: -40px;">
        <ul class="clear">
          <li class="active"><a href="index.php">Home</a></li>
          <li><a class="drop" href="#">Services
    </a>
            <ul>
              <li><a href="#">HANDLING OF INWARD CLEARING</a></li>
              <li><a href="#">CAPTURE IMAGE OF CTS CHQUES</a></li>
              <li><a href="#">HANDLING OF INT./DIVIDEND WARRANTS.</a></li>
              <li><a href="#">PDC MANAGEMENT.(POST DATED CHEQUES)</a></li>
              <li><a href="#">HANDLING OF OUTWARD CLEARING.(CTS)</a></li>
            </ul>
          </li>
          <li><a href="about.php">About us</a></li>
          <li><a class="drop" href="#">Utilities
    </a>
            <ul>
              <li><a href="attachments/gscop.pdf">About GS Cop</a></li>
              <li><a href="product.php">Products</a></li>
             
            </ul>
          </li>
         <!--  <li><a href="#">Achievements</a></li> -->
          <li><a href="contact-us.php">Contact Us</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ --> 
    </header>
  </div>
</div>
<!-- <div class="wrapper row2">
  <div id="breadcrumb" class="clear">
    
    <ul>
      <li><a href="#">Home</a></li>
      <li><a href="#">Lorem</a></li>
      <li><a href="#">Ipsum</a></li>
      <li><a href="#">Dolor</a></li>
    </ul>
   
  </div>
</div> -->
<div class="wrapper row3" style="background-color: #f1f1f1; width: 100%;">
  <main class="" style="margin: 0px;"> 
    <!-- main body --> 
    <!-- ################################################################################################ -->
    <div class="group btmspace-50 demo">
      
      <div>
        <div class="three_quarter first">
           <img id="mce-3816" style="display: block; margin-left: auto; margin-right: auto; width: 90%; margin-top: 90px;" src="admin/assets/img/greetings/<?php echo isset($profile1['images'])?$profile1['images']:"images"; ?>" alt="" width="900px" height="auto">
        </div>
        <div class="one_quarter">
           <h2 style="margin-top:101px;">QUERY SUBMISSION</h2>
             <form id="" class="" action="php/querymail.php" method="post" style="margin-left: 25px;">
                                                                           <div class="form-item"><label class="">Name <span class="form-required">*</span></label>
                                                                              <input type="text" id="" name="name" class="" required>
                                                                           </div>
                                                                           <div class="form-item"><label class="form-item-label">Mobile <span class="form-required">*</span></label>
                                                                              <input type="text"  name="field1" class="" required>
                                                                           </div>
                                                                           <div class="form-item"><label class="form-item-label">Your Address</label>
                                                                              <input type="text" name="field2" class="" required>
                                                                           </div>
                                                                           <div class="form-item"><label class="form-item-label">E-mail <span class="form-required">*</span></label>
                                                                              <input type="text"  name="mail" class="form-item-textfield required email" required>
                                                                           </div>
                                                                           <div class="form-item"><label class="form-item-label">Your Query <span class="form-required">*</span></label><textarea name="message" class="form-item-textarea expand100-200 required"></textarea>
                                                                           </div>
                                                                           <div class="form-note">Fields marked with <span class="form-required">*</span> are required.</div>
                                                                           <input type="submit" value="Send e-mail">
                                                                        </form>

          </div>
        </div>
         
         
          
      </div>
   
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>

<div class="wrapper row5">
  <div id="copyright" class="clear"> 
    <!-- ################################################################################################ -->
   <p style="text-align: center;">&copy; 1989-2017. GS COP Ltd . All Rights Reserved.</p>
    <!-- ################################################################################################ --> 
  </div>
</div>
<!-- JAVASCRIPTS --> 




</body>
</html>