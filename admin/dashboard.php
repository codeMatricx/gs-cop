﻿<?php 
include "database.php";
$sql1 ="SELECT name, mobile,image, heading from profile";
$result1 = $conn->query($sql1);
if($result1->num_rows > 0)
{
   $profile = $result1->fetch_assoc();
}
 ?>
<div id="wrap">
        <!-- HEADER SECTION -->
        <div id="top">

            <nav class="navbar navbar-inverse navbar-fixed-top " style="padding: 10px 0px;">
                <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
                    <i class="icon-align-justify"></i>
                </a>
                <!-- LOGO SECTION -->
                <header class="navbar-header" style="margin-left:20px;">
                    <!-- <img src="assets/img/LIC.png" alt="LICINDIANCR" class="headerimage" /> -->
                    <h1 class="" style="font-size: 40px;"><span style="color: #d1234f;">G</span>SCOP</h1>


                </header>
                <!-- END LOGO SECTION -->


                <ul class="nav navbar-top-links navbar-right">

                   

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="icon-user "></i>&nbsp; <i class="icon-chevron-down "></i>
                        </a>

                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="profile"><i class="icon-user"></i> User Profile </a> </li>
                    </li>
                            <li class="divider"></li>
                            <!-- <li><a href="login.html"><i class="icon-signout"></i> Logout </a>
                            </li> -->
                            <li>
                                <form method="post" class="logoutform">
                                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<i class="icon-signout"></i>
                                    <input class="logout" type="submit" name="logout" Value="logout">
                                </form>
                                <?php
                                    if(isset($_POST['logout']))
                                    {
                                        unset($_SESSION['user']);
                                        session_destroy();
                                        echo '<meta http-equiv="refresh" content="0">';
                                    }?>
                            </li>
                        </ul>

                    </li>
                    <!--END ADMIN SETTINGS -->
                </ul>

            </nav>

        </div>
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
        <div id="left">
            <div class="media user-media well-small">
                <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" height="200px" width="auto" alt="User Picture" src="assets/img/profile/<?php echo $profile['image']; ?>" />
                </a>
                <br />
                <div class="media-body">
                    <h5 class="media-heading"> <?php echo $profile['name']; ?></h5>
                </div>
                <br />
            </div>
            <ul id="menu" class="collapse">
                <li><a href="profile"><i class="icon-user"></i> Profile</a></li>
               <!--  <li><a href="images"><i class="icon-picture"></i> Images</a></li>
                <li><a href="news"><i class="icon-file-text"></i> News</a></li> -->
               <!--  <li><a href="team"><i class="icon-group"></i> Our Teams</a></li> -->
                <li><a href="product"><i class="icon-archive"></i> Product</a></li>
                <li><a href="customer"><i class="icon-smile"></i> Customer</a></li>
                <li><a href="greetings"><i class="icon-picture"></i> Greetings</a></li>
               <!--  <li><a href="licproduct"><i class="icon-picture"></i> LIC-product</a></li> -->
                <li><a href="">
                    <form method="post" class="logoutform">
                        <i class="icon-signout"></i>
                        <input class="logout" type="submit" name="logout" Value="Logout">
                    </form>
                    <?php
                        if(isset($_POST['logout']))
                        {
                            unset($_SESSION['user']);
                            session_destroy();
                            echo '<meta http-equiv="refresh" content="0">';
                        }?>
                    </a>    
                </li>
            </ul>
        </div>
        <!--END MENU SECTION -->
        <!--PAGE CONTENT -->
        <div id="content">
            <?php 
                           if (isset($_GET['images'])) {
                                include "pages/images.php";
                            }
                            elseif (isset($_GET['news'])) {
                                include "pages/news.php";
                            }
                            elseif (isset($_GET['team'])) {
                                include "pages/team.php";
                            }
                            elseif (isset($_GET['product'])) {
                                include "pages/product.php";
                            }
                            elseif(isset($_GET['customer']))
                            {
                                include "pages/customer.php";
                            }
                            elseif (isset($_GET['greetings'])) {
                                include "pages/greetings.php";
                            }
                            elseif (isset($_GET['licproduct'])) {
                                include "pages/licproduct.php";
                            }                           
                            else
                            {
                                
                               include "pages/profile.php";
                            }
                         ?>
         <div style="clear:both;"></div>
        </div>
    </div>
    <!--END MAIN WRAPPER -->
    <!-- FOOTER -->
    <div id="footer">
        <p>© 1989-2017. GS COP Ltd . All Rights Reserved.</p>
    </div>
  <span id="message" style="position: fixed; top:0px; text-align: center; height: auto; padding:10px 0px; width: 400px; left: calc(50% - 200px);z-index: 3000; border:1px solid black;"><?php if(isset($responseMessage)) echo $responseMessage; ?></span>