 <?php 
 function test_input($data)
    {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
if (isset($_POST['addLicproduct'])) {
    $name = test_input($_POST["name"]);
    $link = test_input($_POST["link"]);
    $status = 1;
    if (empty($name) || empty($link)) {
        $status=0;
    }
    if ($status) {
        $sql = "INSERT INTO licproduct (name,link) VALUES ('$name','$link')";
        if ($conn->query($sql) === TRUE)
        {
           $responseMessage =  "LIC-Product Add successfully";
        }
        else
        {
            $responseMessage =  "Connection failed: " . $conn->connect_error;
        }
    }
}
if (isset($_POST['deleteLicproduct']))
{
    $id = test_input($_POST['id']);
    $sql = "DELETE FROM licproduct WHERE id=$id";
    if ($conn->query($sql) === TRUE)
    {
       $success =  "News Remove successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->connect_error;
    }
}
if (isset($_POST['updateLicproduct'])) {
    $name = test_input($_POST["name"]);
    $link = test_input($_POST["link"]);
    $id = test_input($_POST['id']);
    $status = 1;
    if (empty($name) || empty($link) || empty($id)) {
        $status=0;
    }
    if ($status) 
    {
        $sql = "UPDATE licproduct SET name = '$name', link = '$link' WHERE id = $id";
        if ($conn->query($sql) === TRUE)
        {
           $responseMessage =  "LIC-Product edit successfully";
        }
        else
        {
            $responseMessage =  "Connection failed: ".$conn->error;
        }
    }
}
  ?>

<div class="inner" style="min-height: 500px;">
    <div class="row">
        <div class="col-lg-12">

            <h2 style="margin-top: 25px;"> News </h2>
            <input type="text" id="searchfor" placeholder="Search Here.." title="Type in a name" style=" position: absolute; width: 191px;left: 700px; margin-top: -36px;">

                <button id="popup" class="btn text-muted text-center btn-success" onclick="div_show('addLicproduct')" style="width: 90px; margin-top: -42px;">Add New</button>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-lg-12">
            <div class="">

                <div class="">
                    <div class="table-responsive" style="position: absolute; left: 8px; width: 99%;">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">Serial No.</th>
                                    <th style="text-align: center;">Name</th>
                                    <th style="text-align: center;">link</th>
                                    <th style="text-align: center;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                    $sql = "SELECT * from licproduct";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows>0)
                                    {
                                        $serial=1;
                                        
                                        while($licproduct = $result->fetch_assoc())
                                        {
                                            
                                    ?>
                                    <tr class="tosearch" id="<?php  echo $licproduct['id'];?>">
                                        <td style="text-align: center;"><?php echo $serial; ?></td>
                                        <td style="text-align: left;" class="name"><?php  echo $licproduct['name'];?></td>
                                        <td style="text-align: center;" class="link"><?php  echo $licproduct['link'];?></td>
                                        <td style="font-size: 15px; text-align: center">
                                            <a class="<?php  echo $licproduct['id'];?>" onclick="div_show('updateLicproduct',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">UPDATE</a>/
                                            <a class="<?php  echo $licproduct['id'];?>" onclick="div_show('deleteLicproduct',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">DELETE</a></td>
                                    </tr>
                                    <?php
                                        $serial++;
                                         } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="addLicproduct">
                    <!-- Popup Div Starts Here -->
                    <div id="popupAdd" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('addLicproduct')">
                        <form id="form" method="post" name="form">
                            
                            <h2>Add New LIC-Product</h2>
                            <hr>
                            <input id="name" name="name" placeholder=" Product Link Name" type="text">
                            <input id="link" name="link" placeholder="product Link" type="text">
                            <input type="submit" id="submit" name="addLicproduct" value="Add">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                <div id="updateLicproduct">
                    <!-- Popup Div Starts Here -->
                    <div id="popupUpdate" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('updateLicproduct')">
                        <form  id="form" method="post" name="form">
                            
                            <h2>Update LIC-Product</h2>
                            <hr>
                            <input id="updatename" name="name" placeholder=" Product Link Name" type="text">
                            <input id="updatelink" name="link" placeholder="product Link" type="text">
                            <input id="updateId" type="hidden" name="id">
                            <input type="submit" id="submit" name="updateLicproduct" value="Update">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                 <!-- Display Popup Button -->
                <div id="deleteLicproduct">
                    <!-- Popup Div Starts Here -->
                    <div id="popupDelete" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('deleteLicproduct')">
                        <form method="post">
                            <hr>
                            <h2>Are You Sure??</h2>
                            <input type="submit" name="deleteLicproduct" value="OK">
                            <input type="hidden" name="id" id="deleteId">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                <!--POP-->
            </div>
        </div>
    </div>

</div>