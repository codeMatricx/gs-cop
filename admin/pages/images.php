<?php
$target_dir = "assets/img/achievements/";
function test_input($data)
    {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
if(isset($_POST['addImage']))
{
    $target_file = $target_dir . basename($_FILES["image"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if($check == false)
    {
        $uploadOk = 0;
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" )
    {
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) 
    {
        $responseMessage = "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } 
    else
    {
        if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
        {
            $name = $_FILES['image']['name'];
            $sql = "INSERT INTO images (name) VALUES ('$name')";
            if ($conn->query($sql) === TRUE)
            {
               $responseMessage =  "Image Add successfully";
            }
            else
            {
                $responseMessage =  "Connection failed: " . $conn->connect_error;
            }
        }
        else
        {
            $responseMessage =  "Sorry, there was an error in uploading your file.";
        }
    }
}
if (isset($_POST['deleteImage']))
{
  $id = test_input($_POST['id']);
  $sql = "SELECT name from images where id = $id";
  $result = $conn->query($sql);
  if ($result->num_rows>0)
  {
    $image = $result->fetch_assoc();

    $name = $image['name'];
  }
  $sql = "DELETE FROM images WHERE id=$id";
  if ($conn->query($sql) === TRUE)
  {
     unlink($target_dir.$name);
     $responseMessage =  "Image Remove successfully";
  }
  else
  {
      $responseMessage =  "Connection failed: " . $conn->connect_error;
  }
}
?>  
  <div class="inner" style="min-height: 500px;">
    <div class="row">
      <div class="col-lg-12">
        <h2 style="margin-top: 25px;"> Images Gallery </h2>
      <form  method="post" enctype="multipart/form-data"> 
              <input type="submit" name="addImage" style="display:none" id="addImage">
              <label for="imageInput" class="btn text-muted text-center btn-success" style=" margin-top: -49px; margin-left: 980px;">Add Images</label>
              <input id="imageInput" type="file" style="display:none" name="image" onchange="document.getElementById('addImage').click()">
          </form>
    
    
    
    </div>
    </div>
    <hr />
    <!--images-->
    <div class="row">
      <div class="col-lg-12">
        <div class="panel-heading">
          <div class="panel-body"> 
              <?php 
              $sql = "SELECT * from images";
              $result = $conn->query($sql);
              if ($result->num_rows>0)
              {
                $serial=1;
                while($image = $result->fetch_assoc())
                {
                  $id = $image['id'];
              ?>           
                  <div class="col-md-4" style="margin-top: 10px;">
                  <div class="center-cropped">
                    <img style="cursor: pointer;" onclick="div_show('imageview',$(this).attr('src'))" src="<?php echo $target_dir.$image['name']; ?>" alt=""/>
                    </div>
                    <a style="margin-top: 5px;" onclick="div_show('deleteImage',<?php echo $image['id']; ?>)" class='btn btn-danger'>Delete</a>
                    
                  </div> 
              <?php 
                $serial++;
                }
              }
               ?>       
          </div>
        </div>
        <div id="imageview" >
          <div id="popupimage" class="popup">
            <img id="close" src="assets/img/close.png" onclick="div_hide('imageview')">
            <div>
              <img id="imageviewer" src="" height="auto" width="700px">
            </div>
          </div>

          </div>
          <div id="deleteImage">
            <div id="popupimage" class="popup">
              <img id="close" src="assets/img/close.png" onclick="div_hide('deleteImage')">
                  <form method="post">
                      <hr>
                      <h2>Are You Sure??</h2>
                      <input type="submit" name="deleteImage" value="OK">
                      <input type="hidden" name="id" id="deleteId">
                  </form>
            </div>
          </div>
      </div>
    </div>
  <!--end-images-->
  </div>