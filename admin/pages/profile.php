<?php
$target_dir = "assets/img/profile/";
function test_input($data)
    {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
if(isset($_POST['updateProfile']))
{    
    $name = test_input($_POST['name']);
    $email = test_input($_POST['email']);
    $mobile = test_input($_POST['mobile']);
    $heading = test_input($_POST['heading']);
    $set = "";
    $status = 1;
        if (empty($name) || empty($email) || empty($mobile) || empty($heading) )
        {
            $status=0;
        }
        if (!empty($_FILES['image']['name']) && $status)
        {
            $imagename = $_FILES['image']['name'];

            $target_file = $target_dir . basename($_FILES["image"]["name"]);
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            // Check if image file is a actual image or fake image
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if($check == false)
            {
                $status = 0;
            }
            // Check if file already exists
            if (file_exists($target_file)) {
                $status = 0;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" )
            {
                $status = 0;
            }
            if($status)
            {
                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
                {
                    $set .="image = '$imagename',"; 
                } 
            }     
        }
        if ($status)
        {
            $set .= "name = '$name',email = '$email',mobile = '$mobile',heading = '$heading'";
            $sql = "UPDATE profile SET $set WHERE id = '1' ";
            if ($conn->query($sql) === TRUE)
            {
               $responseMessage =  "User Information Updated successfully";
            }
            else
            {
                $responseMessage =  "Connection failed: " . $conn->connect_error;
            }
        } 
    
}
if(isset($_POST['updateImage']))
{
    $imagename = test_input($_POST['image']);
    $set ="image = '$imagename'"; 
    $sql = "UPDATE profile SET $set WHERE id = '1' ";
    if ($conn->query($sql) === TRUE)
    {
       $responseMessage =  "Image Update successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->error;
    }
}
if(isset($_POST['updatePassword']))
{
    $oldpassword = test_input($_POST['currentPassword']);
    $newpassword = test_input($_POST['newPassword']);
    $confirmpassword = test_input($_POST['confirmPassword']);
    $status = 1;
    if ($newpassword == $confirmpassword )
    {
        if (empty($oldpassword) || empty($newpassword) || empty($confirmpassword) )
        {
            $status=0;
        }   
        if ($status)
        {
             $sql = "UPDATE profile SET password = '$newpassword' WHERE password = '$oldpassword' ";
             if ($conn->query($sql) === TRUE)
            {
               $responseMessage =  "Password Updated successfully";
            }
            else
            {
                $responseMessage =  "Connection failed: " . $conn->connect_error;
            }
        } 
    }
}
?>
<div class="inner" style="min-height: 500px;">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="text-center">Profile</h1>
        </div>
    </div>
    <div class="row profileRow">
        <div class="col-sm-2 profileview"></div>
        <div class="col-sm-2 profileview">
            
            <?php 
                $sql = "SELECT * from profile";
                $result = $conn->query($sql);
                if ($result->num_rows>0)
                {
                    $serial=1;
                    $profile = $result->fetch_assoc();  
            ?>
            <img src="<?php echo $target_dir.$profile['image'] ;?>" style="width:200px;height:auto; border:5px solid grey;">
        </div>
        <div class="col-sm-4 profileview">

            <div style="color: black; margin-left: 10px;">

                <table class="space" style="border-collapse: separate; border-spacing: 15px;">
                    <tr>
                        <th>Name:-</th>
                        <td id="name"> <?php echo $profile['name']; ?> </td>
                    </tr>
                    <tr style="">
                        <th>Email:-</th>
                        <td id="email"> <?php echo $profile['email'] ?> </td>
                    </tr>
                    <tr>
                        <th>MOBILE NO.</th>
                        <td id="mobile"> <?php echo $profile['mobile'] ?></td>
                    </tr>
                    <tr>
                        <th>About Me:-</th>
                        <td id="heading"> <?php echo $profile['heading']; ?></td>
                    </tr>
                </table>
                <div style="padding-left: 12px; margin-top: 15px;">
                    <a   onclick="div_show('updatePassword')" style="cursor: pointer;">Change Password</a><br>
                    <a   onclick="div_show('updateProfile')" style="cursor: pointer;">Edit Profile</a>
                </div>
                <div id="updatePassword">
                    <!-- Popup Div Starts Here -->
                    <div id="popupUpdate" class="popup">
                        <!-- Contact Us Form -->
                        <form id="form" method="post" name="form">
                            <img id="close" src="assets/img/close.png" onclick="div_hide('updatePassword')">
                            <h2>Change Password</h2>
                             <hr>
                                <input name="currentPassword" placeholder="Currenet Password" type="password">
                                <hr>
                                <input name="newPassword" placeholder="New Password" type="password">
                                <input name="confirmPassword" placeholder="Confirm-Password" type="password">
                                 <!--<textarea id="msg" name="message" placeholder="Message"></textarea>-->
                                <input type="submit" id="submit" name="updatePassword" value="Update">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>                
                <div id="updateProfile" >
                    <div id="popupUpdate" class="popup">
                    <img id="close" src="assets/img/close.png" onclick="div_hide('updateProfile')">
                        <form id="form" method="post" name="form" enctype="multipart/form-data">
                            
                            <h2>Update Profile</h2>
                            <hr>
                            <input type="text" name="name" id="updatename" placeholder="Name">
                            <input id="updateemail" name="email" placeholder="Email" type="text">
                            <input id="updatemobile" name="mobile" placeholder="mobile no." type="text">
                            <input id="updateheading" name="heading" placeholder="Heading" type="text">
                            <label for="imageInput" class="btn text-muted text-center btn-success" style="width:82%;margin-top: 10px;">change Images</label>
                            <input id="imageInput" type="file" style="display:none" name="image">
                            <input type="submit" id="submit" name="updateProfile" value="Update">
                        </form>
                    </div>

                </div>
                <div id="updateImage">
                    <div id="popupUpdate" class="popup">
                    <img id="close" src="assets/img/close.png" onclick="div_hide('updateImage')">
                        <form id="form" method="post" name="form">
                            
                            <h2>Update Profile</h2>
                            <hr>
                            <input type="hidden" name="image" id="fetchimage">
                            <input type="submit" id="submit" name="updateImage" value="Update">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 profileview"></div>
        <?php } ?>
    </div>
    <div id="galleryimages">
        <button id="imageleft"><</button>
        <div class="">
        <?php 
            $dir    = 'assets/img/profile/';
            $galleryimages = scandir($dir);
         
            array_shift($galleryimages);
            array_shift($galleryimages);
            foreach ($galleryimages as $value) {
                $ext = pathinfo($value, PATHINFO_EXTENSION);
               if ($ext=='jpg' || $ext=='jpeg' || $ext=='png' || $ext=='gif')
               {
                    if($value != $profile['image'])
                    {
                        ?>
                        <img class='galleryimage' src='<?php echo $dir.$value; ?>' width='300px' height='300px' onclick="div_show('updateImage','<?php echo $value; ?>')">
                        <?php
                    }
               }
            }

        ?>
        </div>
        <button id="imageright">></button>
    </div>


</div>
