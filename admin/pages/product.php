 <?php
 $image_dir = "assets/img/products/";
 $demo_dir = "assets/products/demo/";
 $full_dir = "assets/products/full/";
 function test_input($data)
    {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
if (isset($_POST['addProduct'])) {
    $name = test_input($_POST["name"]);
    $details = test_input($_POST["details"]);
    $price = test_input($_POST["price"]);
    $date = test_input($_POST["date"]);
    $status = 1;

    $image_file = $image_dir . basename($_FILES["image"]["name"]);
    $imageFileType = strtolower(pathinfo($image_file,PATHINFO_EXTENSION));
    //demo
    $demo_file = $demo_dir . basename($_FILES["demo"]["name"]);
    $demoFiletype = strtolower(pathinfo($demo_file,PATHINFO_EXTENSION));
    // full version
    $full_file = $full_dir . basename($_FILES["full"]["name"]);
    $fullFiletype = strtolower(pathinfo($full_file,PATHINFO_EXTENSION));

    // Check if image file is a actual image or fake image
    $check = getimagesize($_FILES["image"]["tmp_name"]);

    if($check == false)
    {
        $status = 0;
    }
    // Check if file already exists
    if (file_exists($image_file) && file_exists($demo_file) && file_exists($full_file)) {
        $status = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" && $demoFiletype != "zip" && $fullFiletype != "zip")
    {
        $status = 0;
    }
    // Check if $status is set to 0 by an error
    if ($status == 0) 
    {
        $responseMessage = "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } 
    else
    {
        $imageupload = move_uploaded_file($_FILES["image"]["tmp_name"], $image_file);
        $demoupload = move_uploaded_file($_FILES["demo"]["tmp_name"], $demo_file);
        $fullupload = move_uploaded_file($_FILES["full"]["tmp_name"], $full_file);
        if ($imageupload && $demoupload && $fullupload)
        {
            $image = $_FILES['image']['name'];
            $demo = $_FILES['demo']['name'];
            $full = $_FILES['full']['name'];
            $sql = "INSERT INTO products (name,image,details,price,date,demo_product,full_product) VALUES ('$name','$image','$details','$price','$date','$demo','$full')";
            if ($conn->query($sql) === TRUE)
            {
               $responseMessage =  "Product Add successfully";
            }
            else
            {
                $responseMessage =  "Connection failed: " . $conn->connect_error;
            }
        }
        else
        {
            $responseMessage =  "Sorry, there was an error in uploading your file.";
        }
    }
}
if (isset($_POST['updateProduct'])) {
    $name = test_input($_POST["name"]);
    $details = test_input($_POST["details"]);
    $price = test_input($_POST["price"]);
    $date = test_input($_POST["date"]);
    $id = test_input($_POST['id']);
    $status = 1;
    $imagestatus = 1;
    $demostatus = 1;
    $fullstatus = 1;
    $set = "";
    if (empty($name) || empty($details) || empty($price) || empty($date)) {
        $status=0;
    }
    if (!empty($_FILES['demo']['name']) && $status) {
        $demo = $_FILES['demo']['name'];
        //demo
        $demo_file = $demo_dir . basename($_FILES["demo"]["name"]);
        $demoFiletype = strtolower(pathinfo($demo_file,PATHINFO_EXTENSION));
        if (file_exists($demo_file))
        {
                $demostatus = 0;
        }
        if ($demoFiletype != "zip") {
            $demostatus = 0;
        }
        if ($demostatus) {
            $demoupload = move_uploaded_file($_FILES["demo"]["tmp_name"], $demo_file);
            if ($demoupload)
                {
                    $sql = "SELECT demo_product from products where id = $id";
                    $result = $conn->query($sql);
                    if ($result->num_rows>0)
                    {
                        $data = $result->fetch_assoc();
                        $deletedemoproduct = $data['demo_product'];
                        if (file_exists($demo_dir.$deletedemoproduct)) {
                            @unlink($demo_dir.$deletedemoproduct);
                        }
                    }
                    $set .="demo_product = '$demo',"; 
                }
        }
    }
    if(!empty($_FILES['full']['name']) && $status)
    {
        $full = $_FILES['full']['name'];
        // full version
        $full_file = $full_dir . basename($_FILES["full"]["name"]);
        $fullFiletype = strtolower(pathinfo($full_file,PATHINFO_EXTENSION));
        if (file_exists($full_file))
        {
            $fullstatus = 0;
        }
        if ($fullFiletype != "zip") {
            $fullstatus = 0;
        }

        if ($fullstatus) {
            $fullupload = move_uploaded_file($_FILES["full"]["tmp_name"], $full_file);
            if ($fullupload)
                {
                    $sql = "SELECT full_product from products where id = $id";
                    $result = $conn->query($sql);
                    if ($result->num_rows>0)
                    {
                        $data = $result->fetch_assoc();
                        $deletefullproduct = $data['full_product'];
                        if (file_exists($full_dir.$deletefullproduct)) {
                            @unlink($full_dir.$deletefullproduct);
                        }
                        
                    }
                    $set .="full_product = '$full',"; 
                }
        }
    }
    
    if (!empty($_FILES['image']['name']) && $status)
        {
            $imagename = $_FILES['image']['name'];
            
            
            $image_file = $image_dir . basename($_FILES["image"]["name"]);
            $imageFileType = strtolower(pathinfo($image_file,PATHINFO_EXTENSION));
            
            
            // Check if image file is a actual image or fake image
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if($check == false)
            {
                $imagestatus = 0;
            }
            // Check if file already exists
            if (file_exists($image_file)) {
                $imagestatus = 0;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" )
            {
                $imagestatus = 0;
            }
            if($imagestatus)
            {
                $imageupload = move_uploaded_file($_FILES["image"]["tmp_name"], $image_file);
                if ($imageupload)
                {
                    $sql = "SELECT image from products where id = $id";
                    $result = $conn->query($sql);
                    if ($result->num_rows>0)
                    {
                        $data = $result->fetch_assoc();
                        $oldname = $data['image'];
                        unlink($image_dir.$oldname);
                    }
                    $set .="image = '$imagename',"; 
                }
            }     
        }
    if ($status)
    {
        $set .= "name = '$name', details = '$details', price = '$price', date = '$date' ";
        $sql = "UPDATE products SET $set WHERE id = $id";
        if ($conn->query($sql) === TRUE)
        {
           $responseMessage =  "Product edit successfully";
        }
        else
        {
            $responseMessage =  "Connection failed: " . $conn->connect_error;
        }
    }
}
if (isset($_POST['deleteProduct']))
{
    $id = test_input($_POST['id']);
    $sql = "SELECT image,demo_product,full_product from products where id = $id";
    $result = $conn->query($sql);
    if ($result->num_rows>0)
    {
        $data = $result->fetch_assoc();
        $deleteimage = $data['image'];
        $deleteDemoproduct = $data['demo_product'];
        $deleteFullproduct = $data['full_product'];
    }
    $sql = "DELETE FROM products WHERE id=$id";
    if ($conn->query($sql) === TRUE)
    {
        unlink($image_dir.$deleteimage);
        unlink($demo_dir.$deleteDemoproduct);
        unlink($full_dir.$deleteFullproduct);
        $responseMessage =  "Product Remove successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->connect_error;
    }

}
  ?>
<div class="inner" style="min-height: 500px;">
                <div class="row">
                    <div class="col-lg-12">

                        <h2 style="margin-top: 25px;"> Our Product </h2>
                        <input type="text" id="searchfor" placeholder="Search Here.." title="Type in a name" style=" position: absolute;width: 191px;left: 700px;margin-top: -36px;">

                <button id="popup" class="btn text-muted text-center btn-success" onclick="div_show('addProduct')" style="width: 90px; margin-top: -40px;">Add New</button>
                    </div>

                </div>

                <hr />
                <div class="row">
                    <div class="col-lg-12">
                        <div class="">

                            <div class="">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" style="margin-top: 10px; text-align: center;">
                                        <thead style="">
                                            <tr>
                                                <th>S.No.</th>
                                                <th style="text-align: center;">Picture</th>
                                                <th style="text-align: center;">Product Name</th>
                                                <th style="text-align: center;">Product Detail</th>
                                                <th style="text-align: center;">Product Price</th>
                                                <th style="text-align: center;">Date</th>
                                                <th style="text-align: center;">Demo Product</th>
                                                <th style="text-align: center;">Paid Product</th>
                                                <th style="text-align: center;">Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                <?php 
                                    $sql = "SELECT * from products";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows>0)
                                    {
                                        $serial=1;
                                        
                                        while($products = $result->fetch_assoc())
                                        {
                                            
                                    ?>
                                    <tr class="tosearch" id="<?php  echo $products['id'];?>">
                                        <td style="text-align: center;"><?php echo $serial; ?></td>
                                        <td style="text-align: center;"><img height="50px" width="50px" src="assets/img/products/<?php echo $products['image']; ?>"></td>
                                        <td style="text-align: left;" class="name"><?php  echo $products['name'];?></td>
                                        <td style="text-align: left;" class="details"><?php  echo $products['details'];?></td>
                                        <td style="text-align: center;" class="price"><?php  echo $products['price'];?></td>
                                        <td style="text-align: center;" class="date"><?php  echo $products['date'];?></td>
                                        <td style="text-align: center;" class="date"><?php  echo $products['demo_product'];?></td>
                                        <td style="text-align: center;" class="date"><?php  echo $products['full_product'];?></td>
                                        <td style="font-size: 15px; text-align: center"><a class="<?php  echo $products['id'];?>" onclick="div_show('updateProduct',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">UPDATE</a>/<a class="<?php  echo $products['id'];?>" onclick="div_show('deleteProduct',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">DELETE</a></td>
                                    </tr>
                                    <?php
                                        $serial++;
                                         } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="addProduct">
                    <!-- Popup Div Starts Here -->
                    <div id="popupAdd" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('addProduct')">
                        <form  id="form" method="post" name="form" enctype="multipart/form-data">
                            
                            <h2>Add Product's</h2>
                            <hr>
                            <input id="name" name="name" placeholder=" Product Name" type="text">
                            <input id="details" name="details" placeholder="Product Details" type="text">
                            <input id="price" name="price" placeholder="Product Prices" type="text">
                            <input id="date" name="date" placeholder="Date" type="date">
                            <label for="newimage" class="btn text-muted text-center btn-success" style="width:82%;margin-top: 10px;">Add Image</label>
                            <label for="demo" class="btn  text-center btn-success" style="width:41%;margin-top: 10px;">Add Demo Product</label>
                            <label for="full" class="btn  text-center btn-success" style="width:41%;margin-top: 10px;">Add Full version product</label>
                             <input id="demo" type="file" style="display:none" name="demo">
                             <input id="full" type="file" style="display:none" name="full">
                            <input id="newimage" type="file" style="display:none" name="image">
                            <input type="submit" id="submit" name="addProduct" value="Add">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                <div id="updateProduct">
                    <!-- Popup Div Starts Here -->
                    <div id="popupUpdate" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('updateProduct')">
                        <form  id="form" method="post" name="form" enctype="multipart/form-data">
                            
                            <h2>Update Product's</h2>
                            <hr>
                            <input id="updatename" name="name" placeholder=" Product Name" type="text">
                            <input id="updatedetails" name="details" placeholder="Product Details" type="text">
                            <input id="updateprice" name="price" placeholder="Product Prices" type="text">
                            <input id="updatedate" name="date" placeholder="Date" type="date">
                            <label for="imageInput" class="btn text-muted text-center btn-success" style="width:82%;margin-top: 10px;">change Image</label>
                            <label for="updatedemo" class="btn  text-center btn-success" style="width:41%;margin-top: 10px;">Change Demo Product</label>
                            <label for="updatefull" class="btn  text-center btn-success" style="width:41%;margin-top: 10px;">Change Full version product</label>
                            <input id="updatedemo" type="file" style="display:none" name="demo">
                            <input id="updatefull" type="file" style="display:none" name="full">
                            <input id="imageInput" type="file" style="display:none" name="image">
                            <input id="updateId" type="hidden" name="id">
                            <input type="submit" id="submit" name="updateProduct" value="Update">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                 <!-- Display Popup Button -->
                <div id="deleteProduct">
                    <!-- Popup Div Starts Here -->
                    <div id="popupDelete" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('deleteProduct')">
                        <form method="post">
                            <hr>
                            <h2>Are You Sure??</h2>
                            <input type="submit" name="deleteProduct" value="OK">
                            <input type="hidden" name="id" id="deleteId">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                <!--POP-->
            </div>
        </div>
    </div>           
</div>