 <?php 
 function test_input($data)
    {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
if (isset($_POST['addNews'])) {
    $title = test_input($_POST["title"]);
    $description = test_input($_POST["description"]);
    $link = test_input($_POST["link"]);
    $status = 1;
    if (empty($title) || empty($description) || empty($link)) {
        $status=0;
    }
    if ($status) {
        $sql = "INSERT INTO news (title,description,link) VALUES ('$title','$description','$link')";
        if ($conn->query($sql) === TRUE)
        {
           $responseMessage =  "News Add successfully";
        }
        else
        {
            $responseMessage =  "Connection failed: " . $conn->connect_error;
        }
    }
}
if (isset($_POST['deleteNews']))
{
    $id = test_input($_POST['id']);
    $sql = "DELETE FROM news WHERE id=$id";
    if ($conn->query($sql) === TRUE)
    {
       $success =  "News Remove successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->connect_error;
    }
}
if (isset($_POST['updateNews'])) {
    $title = test_input($_POST["title"]);
    $description = test_input($_POST["description"]);
    $link = test_input($_POST["link"]);
    $id = test_input($_POST['id']);
    $status = 1;
    if (empty($title) || empty($description) || empty($link) || empty($id)) {
        $status=0;
    }
    if ($status) {
        $sql = "UPDATE news SET title = '$title', description = '$description', link = '$link' WHERE id = $id";
        if ($conn->query($sql) === TRUE)
        {
           $responseMessage =  "News edit successfully";
        }
        else
        {
            $responseMessage =  "Connection failed: ".$conn->error;
        }
    }
}
  ?>

<div class="inner" style="min-height: 500px;">
    <div class="row">
        <div class="col-lg-12">

            <h2 style="margin-top: 25px;"> News </h2>
            <input type="text" id="searchfor" placeholder="Search Here.." title="Type in a name" style=" position: absolute; width: 191px;left: 700px; margin-top: -36px;">

                <button id="popup" class="btn text-muted text-center btn-success" onclick="div_show('addNews')" style="width: 90px; margin-top: -42px;">Add New</button>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-lg-12">
            <div class="">

                <div class="">
                    <div class="table-responsive" style="position: absolute; left: 8px; width: 99%;">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">Serial No.</th>
                                    <th style="text-align: center;">Title</th>
                                    <th style="text-align: center;">Description</th>
                                    <th style="text-align: center;">link</th>
                                    <th style="text-align: center;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                    $sql = "SELECT * from news";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows>0)
                                    {
                                        $serial=1;
                                        
                                        while($news = $result->fetch_assoc())
                                        {
                                            
                                    ?>
                                    <tr class="tosearch" id="<?php  echo $news['id'];?>">
                                        <td style="text-align: center;"><?php echo $serial; ?></td>
                                        <td style="text-align: left;" class="title"><?php  echo $news['title'];?></td>
                                        <td style="text-align: left;" class="description"><?php  echo $news['description'];?></td>
                                        <td style="text-align: center;" class="link"><?php  echo $news['link'];?></td>
                                        <td style="font-size: 15px; text-align: center">
                                            <a class="<?php  echo $news['id'];?>" onclick="div_show('updateNews',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">UPDATE</a>/
                                            <a class="<?php  echo $news['id'];?>" onclick="div_show('deleteNews',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">DELETE</a></td>
                                    </tr>
                                    <?php
                                        $serial++;
                                         } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="addNews">
                    <!-- Popup Div Starts Here -->
                    <div id="popupAdd" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('addNews')">
                        <form id="form" method="post" name="form">
                            
                            <h2>Add News</h2>
                            <hr>
                            <input id="title" name="title" placeholder=" News Title" type="text">
                            <input id="description" name="description" placeholder="Description" type="text">
                            <input id="link" name="link" placeholder="Link" type="text">
                            <input type="submit" id="submit" name="addNews" value="Add">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                <div id="updateNews">
                    <!-- Popup Div Starts Here -->
                    <div id="popupUpdate" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('updateNews')">
                        <form  id="form" method="post" name="form">
                            
                            <h2>Update News</h2>
                            <hr>
                             <input id="updatetitle" name="title" placeholder=" News Title" type="text">
                            <input id="updatedescription" name="description" placeholder="Description" type="text">
                            <input id="updatelink" name="link" placeholder="Link" type="text">
                            <input id="updateId" type="hidden" name="id">
                            <input type="submit" id="submit" name="updateNews" value="Update">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                 <!-- Display Popup Button -->
                <div id="deleteNews">
                    <!-- Popup Div Starts Here -->
                    <div id="popupDelete" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('deleteNews')">
                        <form method="post">
                            <hr>
                            <h2>Are You Sure??</h2>
                            <input type="submit" name="deleteNews" value="OK">
                            <input type="hidden" name="id" id="deleteId">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                <!--POP-->
            </div>
        </div>
    </div>

</div>