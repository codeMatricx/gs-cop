<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
    <title>GSCOP Admin Panel</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="assets/css/login.css" />
    <link rel="stylesheet" href="assets/plugins/magic/magic.css" />
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="assets/css/theme.css" />
    <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <!-- <link href="assets/css/layout2.css" rel="stylesheet" />
	<link href="assets/plugins/flot/examples/examples.css" rel="stylesheet" />
	<link rel="stylesheet" href="assets/plugins/timeline/timeline.css" /> -->
	<script src="assets/plugins/jquery-2.0.3.min.js"></script>
    
	<script src="assets/plugins/bootstrap/js/bootstrap.js"></script>
	<!-- <script src="assets/js/login.js"></script> -->
	<!-- <script src="assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script> -->
    <script src="assets/js/main.js"></script>
    <!-- END GLOBAL SCRIPTS -->
    <!-- PAGE LEVEL SCRIPTS -->
   <!--  <script src="assets/plugins/flot/jquery.flot.js"></script>
    <script src="assets/plugins/flot/jquery.flot.resize.js"></script>
    <script src="assets/plugins/flot/jquery.flot.time.js"></script>
    <script src="assets/plugins/flot/jquery.flot.stack.js"></script> -->
</head>
<body>
<?php
require "database.php";
if (isset($_SESSION['user']))
{
	include "dashboard.php";
}
else
{
	include "login.php";
}

?>
</body>
</html>