<?php 
if(isset($_POST['login']))
{
    $status = 1;
    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    if (empty($_POST['email']))
    {
        $emailerr = "*PLease Enter Email Id";
        $status = 0;
    }
    else
    {
        $email = test_input($_POST['email']);
    }
    if (empty($_POST['password'])) {
        $passworderr = "*PLease Enter Password";
        $status = 0;
    }
    else
    {
        $password = test_input($_POST['password']);
    }
    if ($status)
    {
        $sql = "SELECT password,image,name from profile WHERE email = '$email'";
        $result = $conn->query($sql);
        if ($result->num_rows>0)
        {
            $userdata = $result->fetch_assoc();
            if ($userdata['password']== $password)
            {
                $_SESSION['user'] = $email;
                $_SESSION['image'] = $userdata['image'];
                $_SESSION['name'] = $userdata['name'];
                echo '<meta http-equiv="refresh" content="0">';
            }
            else
            {
                $passworderr = "*Incorrect Password";
            }    
            
        }
        else
        {
            $emailerr = "**Enter Valid Email Id and Password";
        }
    }  
}
?>
<div class="container">
    <div class="text-center">
        <h4 style="font-size:40px;"><span style="color:#d1234f; font-size:40px;">G</span>SCOP</h4>
    </div>
    <div class="tab-content" style="width: 600px; margin-left: 270px;">
        <div id="login" class="tab-pane active">
            <form class="form-signin" method="post">
                <p class="text-muted text-center btn-block btn btn-primary btn-rect">
                    Enter your Email-id and password
                </p>
                <input type="email" name="email" placeholder="Email" class="form-control" />
                <input type="password" name="password" placeholder="Password" class="form-control" />
                <input class="btn text-muted text-center btn-primary" type="submit" name="login" value="Sign in">
                <div><?php 
                            if(isset($emailerr))
                            {
                                echo $emailerr;
                                echo "<br>";
                            }
                            if(isset($passworderr))
                            {
                                echo $passworderr;
                            }
                            ?>
                </div>
            </form>
        </div>
        <div id="forgot" class="tab-pane">
            <div class="form-signin">
                <p class="text-muted text-center btn-block btn btn-primary btn-rect">Enter your valid e-mail</p>
                <input id="email" type="email"  required="required" placeholder="Your E-mail"  class="form-control" name="email" />
                <br />
                <input id="mailPassword" type="submit" name="forgetpassword" class="btn text-muted text-center btn-success" value="Recover Password">
            </div>
           
        </div>
        <!-- <div id="signup" class="tab-pane">
            <form action="signupaction.php" class="form-signin">
                <p class="text-muted text-center btn-block btn btn-primary btn-rect">Please Fill Details To Register</p>
                 <input type="text" placeholder="First Name" class="form-control" />
                 <input type="text" placeholder="Last Name" class="form-control" />
                <input type="text" placeholder="Username" class="form-control" />
                <input type="email" placeholder="Your E-mail" class="form-control" />
                <input type="password" placeholder="password" class="form-control" />
                <input type="password" placeholder="Re type password" class="form-control" />
                <button class="btn text-muted text-center btn-success" type="submit">Register</button>
            </form>
        </div> -->
         <div id="mailresponse" style="text-align: center;display: none;"></div>
    </div>
    <div class="text-center">
        <ul class="list-inline">
            <li><a class="text-muted" href="#login" data-toggle="tab">Login</a></li>
            <li><a class="text-muted" href="#forgot" data-toggle="tab">Forgot Password</a></li>
            <!-- <li><a class="text-muted" href="#signup" data-toggle="tab">Signup</a></li> -->
        </ul>
    </div>


</div>
